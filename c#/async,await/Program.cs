﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace async_await
{
    class Program
    {
        static async Task QuickSort(List<int> list){
            if(list.Count<=1)
                return;
            int count=0;
            int tmp=list[0];
            var left=new List<int>();
            var right=new List<int>();
            foreach(var n in list){
                if(n<tmp)
                    left.Add(n);
                else if(n==tmp)
                    ++count;
                else
                    right.Add(n);
            }
            list.Clear();
            var taskL=QuickSort(left);
            var taskR=QuickSort(right);
            await taskL;
            await taskR;
            list.AddRange(left);
            for(int i=0;i<count;i++)
                list.Add(tmp);
            list.AddRange(right);
        }
        static void Main(string[] args)
        {
            var l=new List<int>();
            var R=new Random();
            for(int i=0;i<100000;i++)
                l.Add(R.Next(9999));
            var t= QuickSort(l);
            t.Wait();
                Console.Write($"done");
        }
    }
}
