﻿using System;
using System.Collections.Generic;
namespace Generic_Indexer{
    static class Extender{
        public static void PrintAll<T>(this MyList<T> ml){
            for(int i=0;i<ml.Count;i++)
                Console.WriteLine(ml[i]);
        }

    }
        class MyList<T>{
            
            public delegate void MyEvent(int i,T v);
            public event MyEvent OnSetValue;
            private List<T> list=new List<T>();
            public int Count{
                get{
                    return list.Count;
                }
            }
            public T this[int i]{
                get{
                    return this.list[i];
                }
                set{
                    this.list[i]=value;
                    if(this.OnSetValue!=null){
                        this.OnSetValue(i,value);
                    }
                }

            }
            public void Add(T v){
                this.list.Add(v);
                if(this.OnSetValue!=null){
                    this.OnSetValue(list.Count-1,v);
                }
            }
            public void Remove(T v){
                this.list.Remove(v);
            }
    }
    class Program
    {
        static void Main(string[] args)
        {
            MyList<int> ml=new MyList<int>();
            ml.OnSetValue+=(int i,int v)=>{
                Console.WriteLine($"ml[{i}]={v}");
            };
            ml.Add(1);
            ml.Add(2);
            ml[1]=0;
            ml.PrintAll();
        }
    }
}
