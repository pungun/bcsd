﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace c____
{
    class MyIndexer
    {
        private List<int> list1 = new List<int>();
        public int this[int i] {
            get
            {
                return list1[i] + 3;
            }
            set
            {
                list1[i] = value - 3;
            }
        }
        public void Add(int i) { list1.Add(i); }
    }
    class Indexer
    {
        static void Main(string[] args)
        {
            var l = new MyIndexer();
            l.Add(3);
            Console.WriteLine(l[0]);
            Thread.Sleep(9999);
        }
    }
}
