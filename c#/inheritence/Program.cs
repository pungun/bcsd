﻿using System;

namespace inheritence
{
    interface IRunable{
        void Run();
    }
    abstract class Parent:
    IRunable
    {
        private static int sn=0;
        protected int n=1;
        
        public static void Print(){Console.WriteLine(Parent.sn);}
        abstract public void Run();

    }
    
    class ChildA:
    Parent
    {
        private string str;

        public ChildA(string str=""){
            this.str=str;
        }
        override public void Run(){
            Console.WriteLine($"{str} : {n.ToString()}");
        }
    }
        class ChildB:
    Parent
    {
        private int i;
        public ChildB(int i=0){
            this.i=i;
        }
        override public void Run(){
            Console.WriteLine($"{n.ToString()}+{i.ToString()}={n+i}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var ca=new ChildA("ChildA");
            var cb=new ChildB(3);
            Parent.Print();
            ca.Run();
            cb.Run();

        }
    }
}
