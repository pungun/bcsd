# WINAPI
## 도형그리기 & 마우스
- 텍스트 출력   : TextOut(hdc,x,y,유니코드문자열(L""),문자갯수)
- 사각형 그리기 : Rectangle(hdc,x,y,endx,endy)
- 선 그리기 MoveToEx(hdc,x,y,null) 시작점/ LineTo(hdc,endx,endy) 종료점(LineTo를 연속하면 이전 종료점이 시작점)
- 원 그리기 Elipse(hdc,x,y,endx,endy)(사각형에 꽉차는 원생성)
- 마우스 클릭 이벤트 WM_LBUTTONCDOWN
> lParam에 들어온 4바이트 데이터를 LOWORD,HIWORD 매크로로 x,y값 추출
## 픽메세지&키입력
- peekmessage로 메세지가 있는지 확인한 후, 메세지가 없는 시간에 게임구현
- adjustWindowRect 실제 윈도우의 크기를 판단
- getWindowRect 스크린에서 윈도우의 영역을 얻음
- getClientRect tr을 0,0으로 두고 실제 윈도우 크기를 얻음
- SetWindowPos 윈도우의 크기,위치 조절
## 델타타임
- QueryPerformanceFrequency 측정가능한 시간단위를 받아온다
- QueryPerformanceCount 측정가능한 시간단위로 프로그램 시간을 측정한다.