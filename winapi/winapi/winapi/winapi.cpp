﻿// winapi.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "winapi.h"

#define MAX_LOADSTRING 100
#define _USE_MATH_DEFINES
//구조체 선언
typedef struct _tagFloatRect {
    float left, top, right, bottom;
    float width, height;
}FRECT;
typedef struct _tagFloatCircle {
    float x, y;
    float radius;
}FCIRCLE;
typedef struct _tagBullet {
    FCIRCLE tCR;
    int iSpeed;
    float fAngle;
}BULLET;
typedef struct _tagMonster {
    FCIRCLE tCR;
    int iSpeed;
    float fTime;
    float fLimitTime;
    float fDir;
    bool isLive;
    list<BULLET> bulletList;
}MONSTER;
typedef struct _tagPlayer {
    FCIRCLE tCR;
    int iSpeed;
    bool isLive;
    float fAngle;
    list<BULLET> bulletList;
}PLAYER;
typedef struct _tagGameTime {
    LARGE_INTEGER tSecond;
    LARGE_INTEGER tNow;
    float fDelta;
}GTIME;

// 전역 변수:
HINSTANCE hInst;                                // 현재 인스턴스입니다.
WCHAR szTitle[MAX_LOADSTRING];                  // 제목 표시줄 텍스트입니다.
WCHAR szWindowClass[MAX_LOADSTRING];            // 기본 창 클래스 이름입니다.
HWND g_hWnd;
HDC g_hDC;
PLAYER g_Player;
MONSTER g_Monster;
GTIME g_Time;
int g_iDefaultBulletSpeed;
bool g_bLoop = true;

//상수
enum PLAYER_VALUE {
    PL_SPEED = 300,
    PL_RADIUS = 50,
    PL_ANGLESPEED = 100
};
enum MONSTER_VALUE {
    MO_SPEED = 300,
    MO_RADIUS = 50
};
enum BULLET_VALUE {
    BU_SPEED = 500,
    BU_RADIUS = 5
};
const int BULLET_SPEED = 500;
const float M_PI = 3.141592f;

// 이 코드 모듈에 포함된 함수의 선언을 전달합니다:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

//단위시간 계산
void GetDeltaTime() {
    LARGE_INTEGER tTime;
    QueryPerformanceCounter(&tTime);
    g_Time.fDelta = (tTime.QuadPart - g_Time.tNow.QuadPart) / (float)g_Time.tSecond.QuadPart;
    g_Time.tNow = tTime;
}
//총알 초기위치, 속도 설정
void initPlayerBullet(BULLET* bullet, int speed) {
    (*bullet).tCR.radius = BU_RADIUS;
    (*bullet).fAngle = g_Player.fAngle;
    (*bullet).tCR.x = g_Player.tCR.x + g_Player.tCR.radius*cos((*bullet).fAngle);
    (*bullet).tCR.y = g_Player.tCR.y+ g_Player.tCR.radius*sin((*bullet).fAngle);
    (*bullet).iSpeed = speed;
    
}
void initMonsterBullet(BULLET* bullet, int speed) {
    (*bullet).tCR.radius = BU_RADIUS;
    (*bullet).tCR.x = g_Monster.tCR.x-g_Monster.tCR.radius;
    (*bullet).tCR.y = g_Monster.tCR.y;
    (*bullet).fAngle = 180.f*M_PI/180;
    (*bullet).iSpeed = speed;
}
void moveBullet(BULLET* bullet) {
    float speed = g_Time.fDelta * bullet->iSpeed;
    bullet->tCR.x += speed * cos(bullet->fAngle);
    bullet->tCR.y += speed * sin(bullet->fAngle);

}
//충돌검사
bool colisionBetween(const FRECT  rc1 , const FRECT   rc2) {
    if (rc1.top <= rc2.bottom && rc1.bottom >= rc2.top) {
        if (rc1.left <= rc2.right && rc1.right >= rc2.left) {
            return true;
        }
    }
    return false;
}
bool colisionBetween(const FRECT  rc  , const FCIRCLE cr) {
    bool circleInRect = cr.x >= rc.left&&cr.x <= rc.right&&cr.y >= rc.top&&cr.y <= rc.bottom;
    //원의중심이 사각형 안에 있으면 충돌
    if (circleInRect)
        return true;
    bool circleInEdge = (cr.x < rc.left || cr.x > rc.right) && (cr.y<rc.top&&cr.y > rc.bottom);
    //원의중심이 사각형 밖 외곽에 있고, 모서리와 원심까지의 거리가 원의 반지름보다 작으면 충돌
    float distance_x;
    float distance_y;
    float distance;
    bool isLeft;
    bool isTop;
    if (circleInEdge) {
        isLeft = cr.x < rc.left;
        isTop = cr.x < rc.top;
        distance_x = isLeft ? rc.left - cr.x : cr.x - rc.right;
        distance_y = isTop  ? rc.top - cr.y : cr.y - rc.bottom;
        distance = sqrt(pow(distance_x, 2) + pow(distance_y, 2));
        if (distance < cr.radius)
            return true;
        return false;
    }
    bool isVertical = (cr.x > rc.left) && (cr.x < rc.right);
    distance_x = abs(rc.left+rc.width/2-cr.x);
    distance_y = abs(rc.top+rc.height/2-cr.y);
    if (isVertical) {
        if (distance_y < rc.height + cr.radius)
            return true;
    }
    else {
        if (distance_x < rc.width + cr.radius)
            return true;
    }
    return false;
}
bool colisionBetween(const FCIRCLE cr , const FRECT   rc) {
    return colisionBetween(rc, cr);
}
bool colisionBetween(const FCIRCLE cr1, const FCIRCLE cr2) {
    float distance_x = cr2.x - cr1.x;
    float distance_y = cr2.y - cr1.y;
    float distance = sqrt(pow(distance_x, 2) + pow(distance_y, 2));
    if (distance <= cr1.radius + cr2.radius)
        return true;
    return false;
}
//그리기함수
void drawFloatCircle(HDC hdc,const FCIRCLE cr) {
    Ellipse(hdc, (int)(cr.x - cr.radius), (int)(cr.y - cr.radius), (int)(cr.x + cr.radius), (int)(cr.y + cr.radius));
}
void Run() {
    GetDeltaTime();
    RECT windowRC;
    float fPlayerSpeed = (float)g_Player.iSpeed * g_Time.fDelta;
    float fMonsterSpeed = (float)g_Monster.iSpeed * g_Time.fDelta*g_Monster.fDir;

    GetWindowRect(g_hWnd, &windowRC);

    //플레이어 이동
    if (GetAsyncKeyState('A') & 0x8000) 
        g_Player.tCR.x -= fPlayerSpeed;
    if (GetAsyncKeyState('S') & 0x8000) 
        g_Player.tCR.y += fPlayerSpeed;
    if (GetAsyncKeyState('D') & 0x8000) 
        g_Player.tCR.x += fPlayerSpeed;
    if (GetAsyncKeyState('W') & 0x8000) 
        g_Player.tCR.y -= fPlayerSpeed;

    //플레이어가 윈도우를 넘어가면 제한
    if (g_Player.tCR.x < windowRC.left + g_Player.tCR.radius)
        g_Player.tCR.x = windowRC.left + g_Player.tCR.radius;
    if (g_Player.tCR.y < windowRC.top + g_Player.tCR.radius)
        g_Player.tCR.y = windowRC.top + g_Player.tCR.radius;
    if (g_Player.tCR.x>windowRC.right-g_Player.tCR.radius)
        g_Player.tCR.x = windowRC.right - g_Player.tCR.radius;
    if (g_Player.tCR.y > windowRC.bottom - g_Player.tCR.radius)
        g_Player.tCR.y = windowRC.bottom - g_Player.tCR.radius;
    //앵글 수정
    if (GetAsyncKeyState('R') & 0x8000)
        g_Player.fAngle -= g_Time.fDelta*PL_ANGLESPEED/180*M_PI;
    if (GetAsyncKeyState('F') & 0x8000)
        g_Player.fAngle += g_Time.fDelta*PL_ANGLESPEED / 180 * M_PI;
    //총알 발사
    if (GetAsyncKeyState(VK_SPACE) & 0x8000) {
        BULLET bullet;
        initPlayerBullet(&bullet, BULLET_SPEED);
        g_Player.bulletList.push_front(bullet);
    }
    
    //총알들 이동
    list<BULLET>::iterator iter;
    list<BULLET>::iterator enditer = g_Player.bulletList.end();
    for (iter = g_Player.bulletList.begin(); iter != enditer; iter++)
        moveBullet(&(*iter));
    auto window = windowRC;
    g_Player.bulletList.remove_if([window](BULLET bullet)->bool {return (bullet.tCR.x < window.left)|| (bullet.tCR.y < window.top)|| (bullet.tCR.x > window.right) || (bullet.tCR.y > window.bottom); });
    //몬스터 이동
    g_Monster.tCR.y += fMonsterSpeed;
    if (windowRC.bottom < g_Monster.tCR.y+g_Monster.tCR.radius) {
        g_Monster.tCR.y = (float)windowRC.bottom - g_Monster.tCR.radius;
        g_Monster.fDir = -1.f;
    }
    else if (windowRC.top > g_Monster.tCR.y - g_Monster.tCR.radius) {
        g_Monster.tCR.y = (float)windowRC.top + g_Monster.tCR.radius;
        g_Monster.fDir = 1.f;
    }
    //제한시간이 되면 몬스터 총알 발사
    g_Monster.fTime += g_Time.fDelta;
    if (g_Monster.fTime > g_Monster.fLimitTime) {
        BULLET bullet;
        initMonsterBullet(&bullet, BU_SPEED);
        g_Monster.fTime = 0;
        g_Monster.bulletList.push_front(bullet);
    }
    //몬스터 총알 이동
    enditer = g_Monster.bulletList.end();
    for (iter = g_Monster.bulletList.begin(); iter != enditer; iter++)
        moveBullet(&(*iter));
    g_Monster.bulletList.remove_if([window](BULLET bullet)->bool {return (bullet.tCR.x < window.left) || (bullet.tCR.y < window.top) || (bullet.tCR.x > window.right) || (bullet.tCR.y > window.bottom); });
    //플레이어 충돌검사
    for (iter = g_Monster.bulletList.begin(); iter != enditer; iter++) {
        if (colisionBetween((*iter).tCR, g_Player.tCR))
            g_Player.isLive = false;
    }
    //몬스터 충돌검사
    enditer = g_Player.bulletList.end();
    for (iter = g_Player.bulletList.begin(); iter != enditer; iter++) {
        if (colisionBetween((*iter).tCR, g_Monster.tCR))
            g_Monster.isLive = false;
    }
    //그리기
    TextOut(g_hDC, windowRC.left + 100, windowRC.top + 100, g_Player.isLive ? L"생존" : L"사망", 2);
    TextOut(g_hDC, windowRC.right - 100, windowRC.top + 100, g_Monster.isLive ? L"생존" : L"사망", 2);
    drawFloatCircle(g_hDC, g_Player.tCR);
    MoveToEx(g_hDC, (int)g_Player.tCR.x, (int)g_Player.tCR.y, nullptr);
    LineTo(g_hDC, (int)(g_Player.tCR.x + g_Player.tCR.radius*cos(g_Player.fAngle)),(int)( g_Player.tCR.y + g_Player.tCR.radius*sin(g_Player.fAngle)));
    for (auto bullet : g_Player.bulletList)
        drawFloatCircle(g_hDC,bullet.tCR);
    //몬스터 그리기
    HBRUSH Brush, oBrush;
    Brush = CreateSolidBrush(RGB(50, 50, 50));
    oBrush = (HBRUSH)SelectObject(g_hDC, Brush);
    drawFloatCircle(g_hDC, g_Monster.tCR);
    for (auto bullet : g_Monster.bulletList)
        drawFloatCircle(g_hDC, bullet.tCR);

    SelectObject(g_hDC, oBrush);
    DeleteObject(Brush);
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
    _In_opt_ HINSTANCE hPrevInstance,
    _In_ LPWSTR    lpCmdLine,
    _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 여기에 코드를 입력합니다.

    // 전역 문자열을 초기화합니다.
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_WINAPI, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 응용 프로그램 초기화를 수행합니다:
    if (!InitInstance(hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WINAPI));

    MSG msg;

    //시간정보 초기화
    QueryPerformanceFrequency(&g_Time.tSecond);
    QueryPerformanceCounter(&g_Time.tNow);

    // 기본 메시지 루프입니다:    
    while (g_bLoop)
    {
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else {
            Run();
        }
    }
    ReleaseDC(g_hWnd, g_hDC);
    return (int)msg.wParam;
}



//
//  함수: MyRegisterClass()
//
//  용도: 창 클래스를 등록합니다.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WINAPI));
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = NULL;// MAKEINTRESOURCEW(IDC_WINAPI);
    wcex.lpszClassName = szWindowClass;
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   함수: InitInstance(HINSTANCE, int)
//
//   용도: 인스턴스 핸들을 저장하고 주 창을 만듭니다.
//
//   주석:
//
//        이 함수를 통해 인스턴스 핸들을 전역 변수에 저장하고
//        주 프로그램 창을 만든 다음 표시합니다.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
    hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

    HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

    if (!hWnd)
    {
        return FALSE;
    }
    //전역변수 초기화
    g_hDC = GetDC(g_hWnd);
    g_hWnd = hWnd;
    RECT rc;
    GetWindowRect(g_hWnd, &rc);
    g_Player.iSpeed = PL_SPEED;
    g_Player.isLive = true;
    g_Player.fAngle = 0.f;
    g_Player.tCR = { (float)rc.left - PL_RADIUS,(float)rc.top - PL_RADIUS,PL_RADIUS };
    g_Monster.iSpeed = MO_SPEED;
    g_Monster.fDir = 1.f;
    g_Monster.fTime = 0.f;
    g_Monster.fLimitTime = 1.f;
    g_Monster.isLive = true;
    g_Monster.tCR = { (float)rc.right - MO_RADIUS,(float)rc.top - MO_RADIUS,MO_RADIUS };
    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    return TRUE;
}

//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  용도: 주 창의 메시지를 처리합니다.
//
//  WM_COMMAND  - 응용 프로그램 메뉴를 처리합니다.
//  WM_PAINT    - 주 창을 그립니다.
//  WM_DESTROY  - 종료 메시지를 게시하고 반환합니다.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {

    case WM_COMMAND:
    {
        int wmId = LOWORD(wParam);
        // 메뉴 선택을 구문 분석합니다:
        switch (wmId)
        {
        case IDM_ABOUT:
            DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
            break;
        case IDM_EXIT:
            DestroyWindow(hWnd);
            break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
    }
    break;
    case WM_LBUTTONDOWN:
        break;
    case WM_MOUSEMOVE:
        break;
    case WM_LBUTTONUP:
        break;

    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);
        EndPaint(hWnd, &ps);
    }
    break;
    case WM_KEYDOWN:
        if (wParam == VK_ESCAPE)
            DestroyWindow(hWnd);
        break;
    case WM_DESTROY:
        g_bLoop = false;
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
