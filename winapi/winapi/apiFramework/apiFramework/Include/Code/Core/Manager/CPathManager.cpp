#include "CPathManager.h"

DEFINITION_SINGLE(CPathManager);

CPathManager::CPathManager()
{
}


CPathManager::~CPathManager()
{
}

bool CPathManager::Init()
{
	wchar_t strPath[MAX_PATH] = {};
	GetModuleFileNameW(NULL,(LPWSTR)strPath, MAX_PATH);
	for (int i = lstrlenW(strPath) - 1; i >= 0; --i) {
		if (strPath[i] == '/' || strPath[i] == '\\') {
			memset(strPath + (i + 1), 0,sizeof(wchar_t)*(MAX_PATH-(i+1)));
			break;
		}
	}
	m_mapPath.insert(make_pair(PATH_ROOT, strPath));
	if (!CreatePath(PATH_TEXTURE, L"Texture\\"))
		return false;
	if (!CreatePath("Player", L"Player\\", PATH_TEXTURE))
		return false;
	if (!CreatePath("Button", L"Button\\", PATH_TEXTURE))
		return false;
	if (!CreatePath("Tile", L"Tile\\", PATH_TEXTURE))
		return false;
	return true;
}

bool CPathManager::CreatePath(const string & strKey, const wchar_t * pPath, const string & strBaseKey)
{
	const wchar_t* pBasePath = FindPath(strBaseKey
	);
	wstring strPath;
	if (pBasePath)
		strPath = pBasePath;
	strPath += pPath;
	m_mapPath.insert(make_pair(strKey, strPath));
	return true;
}

const wchar_t * CPathManager::FindPath(const string & strKey)
{
	auto iter = m_mapPath.find(strKey);
	if(iter==m_mapPath.end())
		return nullptr;
	return iter->second.c_str();
}


