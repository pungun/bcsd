#include "CReleaseable.h"
#include "../../Obj/CGObj.h"


CReleaseable::CReleaseable() 
	:m_iRef(0),
	m_bLife(true),
	m_bEnable(true)
{
}


CReleaseable::~CReleaseable()
{
   
}

void CReleaseable::AddRef()
{
     ++m_iRef;
}

int CReleaseable::Release()
{
    --m_iRef;
    if (!m_iRef) {
		delete this;
        return 0;
    }
    return m_iRef;
}
