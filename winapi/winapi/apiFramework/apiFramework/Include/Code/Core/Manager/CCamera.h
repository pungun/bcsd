#pragma once
#include "../Header/game.h"
#define GET_WORLD_RS GET_SINGLE(CCamera)->GetWorldRS()
class CCamera
{
private:
	FPOS			m_tPos;
	RESOLUTION		m_tClientRS;
	RESOLUTION		m_tWorldRS;
	FPOS			m_tPivot;
	class CGObj*	m_pTarget;
	class CGObj*	m_pPrevTarget;

public:
	DECLARE_SINGLE(CCamera)
	void SetTarget(class CGObj* pObj);

	void SetPivot(const FPOS& tPivot) { m_tPivot = tPivot; }
	void SetPivot(float x, float y) { m_tPivot = FPOS(x, y); }

	void SetPos(const FPOS& tPos) { m_tPos = tPos; }
	void SetPos(float x, float y) { m_tPos = FPOS(x, y); }
	FPOS GetPos() const { return m_tPos; }
	FPOS GetLeftTop()const {
		return (FPOS)m_tPos - FPOS(m_tClientRS)*m_tPivot;
	}
	void SetClientResolution(const RESOLUTION& tRS) { m_tClientRS = tRS; }
	void SetClientResolution(float x, float y) { m_tClientRS = RESOLUTION((int)x,(int) y); }
	RESOLUTION GetCllientRS() const { return m_tClientRS; }
	void SetWorldResolution(const RESOLUTION& tRS) { m_tWorldRS = tRS; }
	void SetWorldResolution(float x, float y) { m_tWorldRS = RESOLUTION((int)x, (int)y); }
	RESOLUTION GetWorldRS() const { return m_tWorldRS; }


	bool Init(const FPOS& tPos, const RESOLUTION& tRS,
		const RESOLUTION& tWorldRS);
	void Update(float fDeltaTime);

};

