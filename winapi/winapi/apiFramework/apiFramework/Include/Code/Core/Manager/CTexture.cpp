#include "CTexture.h"
#include"CPathManager.h"
#include"CResourceManager.h"

CTexture::CTexture():
	m_hMemDC(nullptr),
	m_hBitmap(nullptr),
	m_hOldBitmap(nullptr)
{
}


CTexture::~CTexture()
{
	SelectObject(m_hMemDC, m_hOldBitmap);
	DeleteObject(m_hBitmap);
	DeleteDC(m_hMemDC);
}

bool CTexture::LoadTextrue(HINSTANCE hInst, HDC hDC, const string & strKey, const wchar_t * pFileName, const string & strPathKey)
{
	const wchar_t* pPath = GET_SINGLE(CPathManager)->FindPath(strPathKey);
	wstring strPath;
	if (pPath)
		strPath = pPath;
	strPath += pFileName;
	m_hMemDC = CreateCompatibleDC(hDC);
	m_hBitmap = (HBITMAP)LoadImageW(hInst, strPath.c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (!m_hBitmap)
		return false;
	m_hOldBitmap = (HBITMAP)SelectObject(m_hMemDC, m_hBitmap);
	GetObject(m_hBitmap, sizeof(m_tInfo), &m_tInfo);
	return true;
}
