#pragma once
#include "../Header/game.h"


class CCollisionManager{
	DECLARE_SINGLE(CCollisionManager)
private:
	list<class CCollision*>  m_CollisionList;
public:
	bool Init();
	void AddCollision(CCollision* pColl);
	void RemoveCollision(CCollision* pColl);

	bool IsCollided(class CCollisionRect* pColl1, class CCollisionRect* pColl2);

	bool IsCollided(class CCollisionSphere*pColl1, class CCollisionRect*  pColl2);
	bool IsCollided(class CCollisionSphere*pColl1, class CCollisionSphere*pColl2);
	bool IsCollided(class CCollisionRect*  pColl1, class CCollisionSphere*pColl2);

	bool IsCollided(class CCollisionLine*  pColl1, class CCollisionRect*  pColl2);
	bool IsCollided(class CCollisionLine*  pColl1, class CCollisionSphere*pColl2);
	bool IsCollided(class CCollisionLine*  pColl1, class CCollisionLine*  pColl2);
	bool IsCollided(class CCollisionRect*  pColl1, class CCollisionLine*  pColl2);
	bool IsCollided(class CCollisionSphere*pColl1, class CCollisionLine*  pColl2);

	bool IsCollided(class CCollisionPixel* pColl1, class CCollisionRect*  pColl2);
	bool IsCollided(class CCollisionPixel* pColl1, class CCollisionSphere*pColl2);
	bool IsCollided(class CCollisionPixel* pColl1, class CCollisionLine*  pColl2);
	bool IsCollided(class CCollisionPixel* pColl1, class CCollisionPixel* pColl2);
	bool IsCollided(class CCollisionRect*  pColl1, class CCollisionPixel* pColl2);
	bool IsCollided(class CCollisionSphere*pColl1, class CCollisionPixel* pColl2);
	bool IsCollided(class CCollisionLine*  pColl1, class CCollisionPixel* pColl2);

	bool IsCollided(class CCollisionPoint* pColl1, class CCollisionRect*  pColl2);
	bool IsCollided(class CCollisionPoint* pColl1, class CCollisionLine*  pColl2);
	bool IsCollided(class CCollisionPoint* pColl1, class CCollisionSphere*pColl2);
	bool IsCollided(class CCollisionPoint* pColl1, class CCollisionPixel* pColl2);
	bool IsCollided(class CCollisionPoint* pColl1, class CCollisionPoint* pColl2);
	bool IsCollided(class CCollisionRect*  pColl1, class CCollisionPoint* pColl2);
	bool IsCollided(class CCollisionLine*  pColl1, class CCollisionPoint* pColl2);
	bool IsCollided(class CCollisionSphere*pColl1, class CCollisionPoint* pColl2);
	bool IsCollided(class CCollisionPixel* pColl1, class CCollisionPoint* pColl2);
	void Collision(float fDeltaTime);
};