#include "CCamera.h"
#include "../../Obj/CGObj.h"
#include "../../Obj/UI/CMouse.h"
#include "CInput.h"

DEFINITION_SINGLE(CCamera)

CCamera::CCamera() :
	m_pTarget(nullptr),
	m_pPrevTarget(nullptr)
{
}


CCamera::~CCamera()
{
	SAFE_RELEASE(m_pTarget);
	SAFE_RELEASE(m_pPrevTarget);
}

void CCamera::SetTarget(CGObj * pObj)
{
	SAFE_RELEASE(m_pTarget);
	m_pTarget = pObj;

	if (m_pTarget)
		m_pTarget->AddRef();
}

bool CCamera::Init(const FPOS & tPos,
	const RESOLUTION & tRS,
	const RESOLUTION & tWorldRS)
{
	m_tPos = tPos;
	m_tClientRS = tRS;
	m_tWorldRS = tWorldRS;
	m_tPivot = FPOS(0.5f, 0.7f);

	return true;
}


void CCamera::Update(float fDeltaTime)
{
	if (KEYDOWN("MouseRClick")) {
		m_pPrevTarget = m_pTarget;
		m_pTarget = nullptr;
	}
	else if (KEYUP("MouseRClick")) {
		m_pTarget = m_pPrevTarget;
		m_pPrevTarget = nullptr;
	}
	if (m_pTarget)
	{
		if (!m_pTarget->GetLife()) {
			SAFE_RELEASE(m_pTarget);
			return;
		}
		FPOS tPos = m_pTarget->GetPos();
		FPOS tLTArea = m_tPivot * FPOS(m_tClientRS);
		FPOS tRBArea = FPOS(m_tWorldRS) - FPOS(m_tClientRS)*(FPOS(1.f, 1.f) - m_tPivot);
		if (tPos.x < tLTArea.x)
			m_tPos.x = tLTArea.x;
		else if (tPos.x >= tRBArea.x)
			m_tPos.x = tRBArea.x;
		else
			m_tPos.x = m_pTarget->GetPos().x;
		if (tPos.y < tLTArea.y)
			m_tPos.y = tLTArea.y;
		else if (tPos.y >= tRBArea.y)
			m_tPos.y = tRBArea.y;
		else
			m_tPos.y = m_pTarget->GetPos().y;
	}
	else {
		FPOS tPos = GET_SINGLE(CInput)->GetMouse()->GetClientPos();
		if (tPos.x < 200)
			m_tPos.x -= (200-tPos.x)*3 * fDeltaTime;
		if (tPos.y < 200)
			m_tPos.y -= (200 - tPos.y) * 3 * fDeltaTime;
		if (tPos.x > m_tClientRS.iW-200)
			m_tPos.x += (tPos.x- m_tClientRS.iW + 200) * 3 * fDeltaTime;
		if (tPos.y > m_tClientRS.iH - 200)
			m_tPos.y += (tPos.y - m_tClientRS.iH + 200) * 3 * fDeltaTime;
		FPOS tLTArea = m_tPivot * FPOS(m_tClientRS);
		FPOS tRBArea = FPOS(m_tWorldRS) - FPOS(m_tClientRS) * (FPOS(1.f, 1.f) - m_tPivot);
		if (m_tPos.x < tLTArea.x)
			m_tPos.x = tLTArea.x;
		else if (m_tPos.x >= tRBArea.x)
			m_tPos.x = tRBArea.x;
		if (m_tPos.y < tLTArea.y)
			m_tPos.y = tLTArea.y;
		else if (m_tPos.y >= tRBArea.y)
			m_tPos.y = tRBArea.y;
	}
}
