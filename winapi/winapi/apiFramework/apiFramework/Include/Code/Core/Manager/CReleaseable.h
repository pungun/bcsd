#pragma once
#include "../Header/game.h"
class CReleaseable
{
protected:
    int m_iRef;
	bool m_bLife;
	bool m_bEnable;
public:
    CReleaseable();
    virtual ~CReleaseable()=0;
    void AddRef();
    int Release();
public:
	virtual void Die() {
		m_bLife = false;
		m_bEnable = false;
	}
	bool GetLife()const {
		return m_bLife;
	}
	void Enable() {
		m_bEnable = true;
	}
	void Disable() {
		m_bEnable = false;
	}
	bool GetEnable()const {
		return m_bEnable;
	}
};

