#pragma once
#include"../Header/game.h"
class CTime
{
private:
	HWND		  m_hWnd;
    LARGE_INTEGER m_tSecond;
    LARGE_INTEGER m_tTime;
    float m_fDelta;
    float m_fTimeScale;
    float m_fFPS;
    float m_fFPSTime;
    int m_iFrame;
    int m_iFrameMax;

public:
    DECLARE_SINGLE(CTime);
    bool Init(HWND hWnd);
    float GetDeltaTime() const;
    void Update();
public:
    void SetTimeScale(float fTimeScale) {
        m_fTimeScale = fTimeScale;
    }
    void SetFrameMax(int iFrameMax) {
        m_iFrameMax = iFrameMax;
    }
    float GetTimeScale()const {
        return m_fTimeScale;
    }
    float GetFPS()const {
        return m_fFPS;
    }
    int GetFrameMax() const {
        return m_iFrameMax;
    }
    int GetFrame() const {
        return m_iFrame;
    }
};

