#pragma once
#include "CReleaseable.h"
class CTexture :
	public CReleaseable
{
private:
	friend class CResourceManager;
private:
	HDC			m_hMemDC;
	HBITMAP		m_hBitmap;
	HBITMAP		m_hOldBitmap;
	BITMAP		m_tInfo;
private:
	CTexture();
	~CTexture();
public:
	bool LoadTextrue(HINSTANCE hInst,HDC hDC,const string& strKey,const wchar_t* pFileName, const string& strPathKey=PATH_TEXTURE);
public:
	BITMAP GetInfo() const{
		return m_tInfo;
	}
	HDC GetDC()const {
		return m_hMemDC;
	}
};

