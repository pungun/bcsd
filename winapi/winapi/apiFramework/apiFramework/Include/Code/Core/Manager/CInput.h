#pragma once
#include"../Header/game.h"

typedef struct _tagKeyInfo {
	string			strName;
	DWORD			KeyCode;
	bool			bCtrl;
	bool			bShift;
	bool			bAlt;
	bool			bDown;
	bool			bPress;
	bool			bUp;
	_tagKeyInfo() :
		bDown(false),
		bPress(false),
		bUp(false),
		bCtrl(false),
		bShift(false),
		bAlt(false),
		KeyCode(0),
		strName("")
	{
	}
}KEYINFO,*PKEYINFO;
class CInput
{
private:
	HWND			m_hWnd;
	POINT			m_tMousePos;
	class CMouse*	m_pMouse;
	PKEYINFO		m_pCreateKey;
	unordered_map<string, PKEYINFO> m_MapKey;
public:
	DECLARE_SINGLE(CInput)
public:
	bool Init(HWND hWnd);
	void Update(float fDeltaTime);
	PKEYINFO FindKey(const string& strKey);

	
public:
	void AddKey(const string& strTag, char code, bool ctrl=false, bool shift=false, bool alt=false);
	void AddCtrlKey(const string&strTag);
	void AddShiftKey(const string&strTag);
	void AddAltKey(const string&strTag);
	bool KeyDown(const string& strKey);
	bool KeyPress(const string& strKey);
	bool KeyUp(const string& strKey);
	class CMouse* GetMouse();
	void SetMouse(class CMouse* pMouse);
};

