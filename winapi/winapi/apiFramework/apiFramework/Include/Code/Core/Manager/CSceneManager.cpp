#include "CSceneManager.h"
#include"../../Screen/Scene/CInGameScene.h"
#include"../../Screen/Scene/CStartScene..h"
#include"../../Obj/Static/CTile.h"
DEFINITION_SINGLE(CSceneManager);

CSceneManager::CSceneManager():
    m_pScene(nullptr),
    m_pNextScene(nullptr)
{
}


CSceneManager::~CSceneManager()
{
    SAFE_DELETE(m_pScene);
	SAFE_DELETE(m_pNextScene);
}

bool CSceneManager::Init()
{
    CreateScene<CStartScene>(SC_CURRENT);
	CTile::AddTileName(TT_DEFAULT, L"Default.bmp");
	CTile::AddTileName(TT_EARTH, L"Earth.bmp");
	CTile::AddTileName(TT_GRASS, L"Grass.bmp");
	CTile::AddTileName(TT_SAND, L"Sand.bmp");
	CTile::AddTileTag(TT_DEFAULT,"Default");
	CTile::AddTileTag(TT_EARTH,"Earth");
	CTile::AddTileTag(TT_GRASS,"Grass");
	CTile::AddTileTag(TT_SAND,"Sand");
    return true;
}



void CSceneManager::Update(float fDeltaTime)
{
	if(m_pScene)
		m_pScene->Update(fDeltaTime);
}

void CSceneManager::LateUpdate(float fDeltaTime)
{
	m_pScene->LateUpdate(fDeltaTime);
}

void CSceneManager::Render(HDC hDC, float fDeltaTime)
{
	if(m_pScene)
		m_pScene->Render(hDC,fDeltaTime);
}

void CSceneManager::ChangeScene()
{
	if (m_pScene)
		SAFE_DELETE(m_pScene);
	m_pScene = m_pNextScene;
	m_pNextScene = nullptr;

}