#pragma once
#include "../Header/game.h"
#include"CTexture.h"
using namespace std;
class CResourceManager
{
private:
	unordered_map<string, CTexture*> m_MapTexture;
	HINSTANCE		m_hInst;
	HDC				m_hDC;
	CTexture*		m_pBackBuffer;
	CTexture*		m_pBuffer;
public:
	CTexture* FindTexture(const string& strKey);
	CTexture* LoadTexture(const string& strKey, const wchar_t* pFileName, const string& strPathKey=PATH_TEXTURE);
public:
	DECLARE_SINGLE(CResourceManager)
	bool Init(HINSTANCE hInst,HDC hDC);
	CTexture* GetBackBuffer() const {
		return m_pBackBuffer;
	}
	CTexture* GetBuffer() const {
		return m_pBuffer;
	}
};

