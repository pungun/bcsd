#include "CResourceManager.h"

DEFINITION_SINGLE(CResourceManager);

CResourceManager::CResourceManager()
{
}


CResourceManager::~CResourceManager()
{
	SAFE_RELEASE_MAP(m_MapTexture);
	SAFE_RELEASE(m_pBackBuffer);
	SAFE_RELEASE(m_pBuffer);
}

CTexture * CResourceManager::LoadTexture(const string & strKey, const wchar_t * pFileName, const string & strPathKey)
{
	CTexture* pTexture = FindTexture(strKey);
	if(pTexture)
		return pTexture;
	pTexture = new CTexture;
	pTexture->AddRef();
	if (!pTexture->LoadTextrue(m_hInst,m_hDC, strKey, pFileName, strPathKey)) {
		SAFE_RELEASE(pTexture);
		return nullptr;
	}
	m_MapTexture.insert(make_pair(strKey, pTexture));
	return pTexture;
}


bool CResourceManager::Init(HINSTANCE hInst,HDC hDC)
{
	m_hInst = hInst;
	m_hDC = hDC;
	CTexture* pBackBuffer = LoadTexture("BackBuffer", L"BackBuffer.bmp");
	CTexture* pBuffer = LoadTexture("Buffer", L"Buffer.bmp");
	pBackBuffer->AddRef();
	pBuffer->AddRef();
	m_pBackBuffer = pBackBuffer;
	m_pBuffer = pBuffer;
	return true;
}

CTexture * CResourceManager::FindTexture(const string & strKey)
{
	auto iter = m_MapTexture.find(strKey);
	if(iter==m_MapTexture.end())
		return nullptr;
	return iter->second;
}

