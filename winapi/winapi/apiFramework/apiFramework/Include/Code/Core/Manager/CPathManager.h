#pragma once
#include"../Header/game.h"
class CPathManager
{
private:
	unordered_map<string, wstring> m_mapPath;
public:
	DECLARE_SINGLE(CPathManager);
	bool Init();
	bool CreatePath(const string& strKey, const wchar_t* pPath, const string& strBaseKey=PATH_ROOT);
	const wchar_t* FindPath(const string& strKey);
};
