#pragma once
#include"../Header/game.h"
#include"../../Screen/CScene.h"
class CSceneManager
{
private:
    class CScene* m_pScene;//현재 장면
    class CScene* m_pNextScene;//다음 장면
public:
    DECLARE_SINGLE(CSceneManager);
public:    
    bool Init();
    void Update(float fDeltaTime);
    void LateUpdate(float fDeltaTime);
    void Render(HDC hDC, float fDeltaTime);
public:
	CScene* GetScene()const {
		return m_pScene;
	}
	CScene* GetNextScene()const {
		return m_pNextScene;
	}
	void ChangeScene();
public:
	template<class T>
    T* CreateScene(SCENE_CREATE sc) {
        T* pScene = new T;
        if (!pScene->Init()) {
            SAFE_DELETE(pScene);
            return nullptr;
        }
        switch (sc) {
        case SC_CURRENT:
            SAFE_DELETE(m_pScene);
            m_pScene = pScene;
            break;
        case SC_NEXT:
            SAFE_DELETE(m_pNextScene);
            m_pNextScene = pScene;
            break;
        }
        return pScene;
    }
};
