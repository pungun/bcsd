#include "CTime.h"
DEFINITION_SINGLE(CTime);


CTime::CTime()
{
}


CTime::~CTime()
{
}

bool CTime::Init(HWND hWnd)
{
	m_hWnd = hWnd;
    QueryPerformanceFrequency(&m_tSecond);
    QueryPerformanceCounter(&m_tTime);
    m_fDelta = 0.f;
    m_fTimeScale = 1.f;
    m_fFPS = 0.f;
    m_fFPSTime = 0.f;
    m_iFrame = 0;
    m_iFrameMax = 60;
    return true;
}

float CTime::GetDeltaTime() const
{
    return m_fDelta*m_fTimeScale;
}

void CTime::Update()
{
    LARGE_INTEGER tTime;
    QueryPerformanceCounter(&tTime);
    m_fDelta = (float)(tTime.QuadPart - m_tTime.QuadPart) / (float)m_tSecond.QuadPart;
    m_tTime = tTime;
	m_iFrame++;
	m_fFPSTime += m_fDelta;
	if (m_fFPSTime >= 0.5f) {
		m_fFPS = (float)m_iFrame / m_fFPSTime;
		m_fFPSTime = 0.f;
		m_iFrame = 0;
#ifdef _DEBUG
		char buffer[64]{};
		sprintf_s(buffer, "%d", (int)m_fFPS);
		SetWindowTextA(m_hWnd, buffer);
#endif
	}
}
