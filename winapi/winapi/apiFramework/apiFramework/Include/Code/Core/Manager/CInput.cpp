#include "CInput.h"
#include"../../Obj/UI/CMouse.h"

DEFINITION_SINGLE(CInput);

CInput::CInput():
	m_pCreateKey(nullptr),
	m_hWnd(nullptr),
	m_pMouse(nullptr),
	m_tMousePos(POINT())
{
}


CInput::~CInput()
{
	SAFE_DELETE_MAP(m_MapKey);
	SAFE_RELEASE(m_pMouse);
}

bool CInput::Init(HWND hWnd)
{
	m_hWnd = hWnd;
	GetCursorPos(&m_tMousePos);
	AddKey("Left", 'A');
	AddKey("Right", 'D');
	AddKey("Up", 'W');
	AddKey("Down", 'S');
	AddKey("Fire",VK_SPACE);
	AddKey("MouseLClick", VK_LBUTTON);
	AddKey("MouseRClick", VK_RBUTTON);
	AddKey("F1", VK_F1);
	AddKey("F2", VK_F2);
	AddKey("F3", VK_F3);
	AddKey("F4", VK_F4);
	AddKey("F5", VK_F5);
	AddKey("F6", VK_F6);
	AddKey("F7", VK_F7);
	return true;
}

void CInput::Update(float fDeltaTime)
{
	RECT rc;
	GetWindowRect(m_hWnd,&rc);
	GetCursorPos(&m_tMousePos);
	m_tMousePos.x -= rc.left+8;
	m_tMousePos.y -= rc.top+32;
	if (m_pMouse) {
		FSIZE tMove=FSIZE(m_tMousePos.x- m_pMouse->GetClientPos().x, m_tMousePos.y-m_pMouse->GetClientPos().y);
		m_pMouse->SetClientPos(m_tMousePos);
		m_pMouse->SetMove(tMove);
	}
	bool IsDowned;
	for (auto Key : m_MapKey) {
		IsDowned = (GetAsyncKeyState(Key.second->KeyCode) & 0x8000);
		if ((GetAsyncKeyState(VK_CONTROL) & 0x8000))
			IsDowned = IsDowned && Key.second->bCtrl;
		else
			IsDowned = IsDowned && !Key.second->bCtrl;
		if ((GetAsyncKeyState(VK_SHIFT) & 0x8000))
			IsDowned = IsDowned && Key.second->bShift;
		else
			IsDowned = IsDowned && !Key.second->bShift;
		if ((GetAsyncKeyState(VK_MENU) & 0x8000))
			IsDowned = IsDowned && Key.second->bAlt;
		else
			IsDowned = IsDowned && !Key.second->bAlt;
		if(IsDowned){
			if (!Key.second->bDown && !Key.second->bPress)
				Key.second->bDown = true;
			else if (Key.second->bDown) {
				Key.second->bDown = false;
				Key.second->bPress = true;
			}
		}else{
			if (Key.second->bDown || Key.second->bPress) {
				Key.second->bDown = false;
				Key.second->bPress = false;
				Key.second->bUp = true;
			}
			else if (Key.second->bUp) {
				Key.second->bUp = false;
			}
		}
	}
}

PKEYINFO CInput::FindKey(const string & strKey)
{
	auto iter = m_MapKey.find(strKey);
	if (iter == m_MapKey.end())
		return nullptr;
	return iter->second;
}

void CInput::AddKey(const string & strTag, char code, bool ctrl, bool shift, bool alt)
{
	auto keyInfo=new KEYINFO;
	keyInfo->strName = strTag;
	keyInfo->bCtrl = ctrl;
	keyInfo->bShift = shift;
	keyInfo->bAlt = alt;
	keyInfo->KeyCode = code;
	m_MapKey.insert(make_pair(keyInfo->strName, keyInfo));
}

void CInput::AddCtrlKey(const string & strTag)
{
	AddKey(strTag, (char)0, true, false, false);
}

void CInput::AddShiftKey(const string & strTag)
{
	AddKey(strTag, (char)0, false, true, false);
}

void CInput::AddAltKey(const string & strTag)
{
	AddKey(strTag, (char)0, false, false, true);
}

bool CInput::KeyDown(const string & strKey)
{
	auto info = FindKey(strKey);
	if (!info)
		return false;
	return info->bDown;
}

bool CInput::KeyPress(const string & strKey)
{
	auto info = FindKey(strKey);
	if (!info)
		return false;
	return info->bPress;
}

bool CInput::KeyUp(const string & strKey)
{
	auto info = FindKey(strKey);
	if (!info)
		return false;
	return info->bUp;
}

CMouse* CInput::GetMouse()
{
	 return m_pMouse; 
}

void CInput::SetMouse(CMouse* pMouse)
{
	if (m_pMouse)
		SAFE_RELEASE(m_pMouse);
	m_pMouse = pMouse;
	m_pMouse->AddRef();
}