#include "CCollisionManager.h"
#include"../../Collision/CCollisionRect.h"
#include"../../Collision/CCollisionSphere.h"
#include"../../Collision/CCollisionLine.h"
#include"../../Collision/CCollisionPixel.h"
#include"../../Collision/CCollisionPoint.h"
#include"../../Obj/CGObj.h"
DEFINITION_SINGLE(CCollisionManager)
CCollisionManager::CCollisionManager() {

}
CCollisionManager::~CCollisionManager() {
	SAFE_RELEASE_VEC_LIST(m_CollisionList);
}
bool CCollisionManager::Init()
{
	return true;
}


void CCollisionManager::AddCollision(CCollision * pColl)
{
	if (!pColl)
		return;
	m_CollisionList.push_back( pColl);
	pColl->AddRef();

}

void CCollisionManager::RemoveCollision(CCollision * pColl)
{
	for (auto iter=m_CollisionList.begin();iter!=m_CollisionList.end();++iter)
		if ((*iter) == pColl) {
			SAFE_RELEASE(pColl);
			m_CollisionList.erase(iter);
			return;
		}
}

bool CCollisionManager::IsCollided(CCollisionRect * pColl1, CCollisionRect * pColl2)
{
	FPOS lt1 = pColl1->GetPos();
	FPOS lt2 = pColl2->GetPos();
	FPOS rb1 = lt1 + pColl1->GetSize();
	FPOS rb2 = lt2 + pColl2->GetSize();
	if (lt1.y <= rb2.y && lt1.y >= lt2.y) {
		if (lt1.x <= rb2.x && rb1.x >= lt2.x) {
			return true;
		}
	}
	return false;
}

bool CCollisionManager::IsCollided(CCollisionRect * pColl1, CCollisionSphere * pColl2)
{
	FPOS lt1 = pColl1->GetPos();
	FPOS rb1 = lt1 + pColl1->GetSize();
	FPOS center = pColl2->GetCenter();
	//원의중심이 사각형 안에 있으면 충돌
	bool SphereInRect = lt1.x <= center.x&&
						center.x<= rb1.x&&
						lt1.y <= center.y&&
						center.y <= rb1.y;
	if (SphereInRect)
		return true;
	//원의중심이 사각형 밖 외곽에 있고, 모서리와 원심까지의 거리가 원의 반지름보다 작으면 충돌
	bool SphereInEdge = (lt1.x > center.x || center.x > rb1.x) &&
						(lt1.y > center.y || center.y > rb1.y);
	if (SphereInEdge) {
		bool InLeft = center.x < lt1.x;
		bool InTop = center.y < lt1.y;
		float DistX = InLeft ? lt1.x - center.x:center.x-rb1.x;
		float DistY = InTop  ? lt1.y - center.y:center.y-rb1.y;
		float Dist  = sqrt(pow(DistX, 2) + pow(DistY, 2));
		if (Dist < pColl2->GetRadius())
			return true;
		return false;
	}
	
	bool InVertical = (lt1.x <= center.x) && (center.x <= rb1.x);
	if (InVertical) {
		float DistY = abs((lt1.y + rb1.y) / 2 - center.y);
		if (DistY < pColl1->GetSize().y + pColl2->GetRadius())
			return true;
	}
	else {
		float DistX = abs((lt1.x + rb1.x) / 2 - center.x);
		if (DistX < pColl1->GetSize().x + pColl2->GetRadius())
			return true;
	}
	return false;
}

bool CCollisionManager::IsCollided(CCollisionSphere * pColl1, CCollisionRect * pColl2)
{
	return IsCollided(pColl2,pColl1);
}

bool CCollisionManager::IsCollided(CCollisionSphere * pColl1, CCollisionSphere * pColl2){
	FPOS center1 = pColl1->GetCenter();
	FPOS center2 = pColl2->GetCenter();
	float DistX = center1.x - center2.x;
	float DistY = center1.y - center2.y;
	float Dist  = sqrt(pow(DistX, 2) + pow(DistY, 2));
	if (Dist <= pColl1->GetRadius() + pColl2->GetRadius())
		return true;
	return false;
}

bool CCollisionManager::IsCollided(CCollisionLine * pColl1, CCollisionRect * pColl2)
{
	return false;
}

bool CCollisionManager::IsCollided(CCollisionRect * pColl1, CCollisionLine * pColl2)
{
	return false;
}

bool CCollisionManager::IsCollided(CCollisionLine * pColl1, CCollisionSphere * pColl2)
{
	return false;
}

bool CCollisionManager::IsCollided(CCollisionSphere * pColl1, CCollisionLine * pColl2)
{
	return false;
}

bool CCollisionManager::IsCollided(CCollisionLine * pColl1, CCollisionLine * pColl2)
{
	if(pColl1->GetLine().Gradient()==pColl2->GetLine().Gradient())
		return false;
	return false;
}

bool CCollisionManager::IsCollided(CCollisionPixel * pColl1, CCollisionRect * pColl2)
{
	PIXEL collpix = { 87,122,185 };
	FPOS start = pColl2->GetPos()- pColl1->GetPos();
	FPOS end = start + pColl2->GetSize();
	auto& pixels=*pColl1->GetPixel();
	for (int y = (int)start.y; y <= (int)end.y; y++) {
		if(y>=0&&y<(int)pixels.size())
		for (int x = (int)start.x; x <= (int)end.x; x++) {
			if(x>=0 && x < (int)pixels[0]->row.size()){
				if (pixels[y]->row[x] == collpix)
					return true;
			}
		}
	}
	return false;
}



bool CCollisionManager::IsCollided(CCollisionPixel * pColl1, CCollisionLine * pColl2)
{
	return false;
}

bool CCollisionManager::IsCollided(CCollisionPixel * pColl1, CCollisionSphere * pColl2)
{
	return false;
}

bool CCollisionManager::IsCollided(CCollisionRect * pColl1, CCollisionPixel * pColl2)
{
	return IsCollided(pColl2, pColl1);
}

bool CCollisionManager::IsCollided(CCollisionLine * pColl1, CCollisionPixel * pColl2)
{
	return false;
}

bool CCollisionManager::IsCollided(CCollisionSphere * pColl1, CCollisionPixel * pColl2)
{
	return false;
}

bool CCollisionManager::IsCollided(CCollisionPixel * pColl1, CCollisionPixel * pColl2)
{
	return false;
}

bool CCollisionManager::IsCollided(CCollisionPoint* pColl1, CCollisionRect* pColl2)
{
	auto Point = pColl1->GetPos();
	auto LeftTop=pColl2->GetPos();
	auto RightBottom=LeftTop+pColl2->GetSize();
	if (Point.x >= LeftTop.x)
		if (Point.x <= RightBottom.x)
			if (Point.y >= LeftTop.y)
				if (Point.y <= RightBottom.y)
					return true;
	return false;
}

bool CCollisionManager::IsCollided(CCollisionPoint* pColl1, CCollisionLine* pColl2)
{
	auto point = pColl1->GetPos();
	auto dist = pColl2->GetLine().Dist();
	auto gradient = pColl2->GetLine().Gradient();
	auto delta = 1.f / (gradient + 1.f);
	float x = pColl2->GetBegin().x;
	float y = pColl2->GetBegin().y;
	float endX = pColl2->GetEnd().x;
	float endY = pColl2->GetEnd().y;
	if ((int)x == (int)point.x && (int)y == (int)point.y)
		return true;
	for (int i = 0; i < dist; i++) {
		x += delta;
		y += gradient * delta;
		if ((int)x == (int)point.x && (int)y == (int)point.y)
			return true;
	}
	return false;
}

bool CCollisionManager::IsCollided(CCollisionPoint* pColl1, CCollisionSphere* pColl2)
{
	FPOS center = pColl2->GetCenter();
	float DistX = center.x - pColl1->GetPos().x;
	float DistY = center.y - pColl1->GetPos().y;
	float Dist = sqrt(pow(DistX, 2) + pow(DistY, 2));
	if (Dist <= pColl2->GetRadius())
		return true;
	return false;
}

bool CCollisionManager::IsCollided(CCollisionPoint* pColl1, CCollisionPixel* pColl2)
{
	PIXEL collpix = { 87,122,185 };
	FPOS target = pColl1->GetPos() - pColl2->GetPos();
	auto& pixels = *pColl2->GetPixel();
	if ((int)target.y >= 0 && (int)target.y <= (int)pixels.size())
		if ((int)target.x >= 0 && (int)target.x <= (int)pixels[0]->row.size())
			if (pixels[(int)target.y]->row[(int)target.x] == collpix)
				return true;
	return false;
}

bool CCollisionManager::IsCollided(CCollisionPoint* pColl1, CCollisionPoint* pColl2)
{
	if ((int)pColl1->GetPos().x == (int)pColl2->GetPos().x && (int)pColl1->GetPos().y == (int)pColl2->GetPos().y)
		return true;
	return false;
}

bool CCollisionManager::IsCollided(CCollisionRect* pColl1, CCollisionPoint* pColl2)
{
	return IsCollided(pColl2, pColl1);
}
bool CCollisionManager::IsCollided(CCollisionLine* pColl1, CCollisionPoint* pColl2)
{
	return IsCollided(pColl2, pColl1);
}

bool CCollisionManager::IsCollided(CCollisionSphere* pColl1, CCollisionPoint* pColl2)
{
	return IsCollided(pColl2, pColl1);
}

bool CCollisionManager::IsCollided(CCollisionPixel* pColl1, CCollisionPoint* pColl2)
{
	return IsCollided(pColl2, pColl1);
}




void CCollisionManager::Collision(float fDeltaTime)
{
	for (auto base : m_CollisionList) {
		if (base->GetEnable()&&base->HasNoTarget())
			continue;
		for (auto target : m_CollisionList) {
			auto pObj = target->GetObj();
			string tag;
			if (pObj)
				tag = pObj->GetTag();
			if (target->GetEnable() && base->IsReactTo(tag))
				if (base->CollisionTo(target))
					base->RunEvent(target);
		}
	}

}
