#pragma once
#define SAFE_DELETE(p) if (p) { delete p; p = nullptr; }
#define SAFE_DELETE_ARRAY(p) if (p) { delete[] p; p = nullptr; }
#define SAFE_DELETE_VEC_LIST(veclist)\
for (auto& element : veclist)\
	SAFE_DELETE(element);\
	veclist.clear()
#define SAFE_DELETE_MAP(map)\
for (auto& element : map)\
	SAFE_DELETE(element.second);\
	map.clear()
#define SAFE_RELEASE(p) if(p){p->Release();p=nullptr;}
#define SAFE_RELEASE_VEC_LIST(veclist)\
for (auto& element : veclist)\
	SAFE_RELEASE(element);\
	veclist.clear()
#define SAFE_RELEASE_MAP(map)\
for (auto& element : map)\
	SAFE_RELEASE(element.second);\
	map.clear()
#define DECLARE_SINGLE(Type)			\
	public:								\
		static Type* m_pInst;				\
		Type();\
		~Type();\
	public:									\
		static Type* GetInst(){\
			if (!m_pInst)\
				m_pInst = new Type;\
			return m_pInst;\
		}\
		static void DestroyInst(){\
			if (m_pInst) { delete m_pInst; m_pInst = nullptr; }\
		}
#define DEFINITION_SINGLE(Type) Type* Type::m_pInst=nullptr;
#define GET_SINGLE(Type) Type::GetInst()
#define DESTROY_SINGLE(Type) Type::DestroyInst()
#define GET_RES CCore::GetInst()->GetResolution()
constexpr auto PATH_ROOT = "PathRoot";
constexpr auto PATH_TEXTURE = "PathTexture";
#define TRANS_COR RGB(0,255,84)
#define KEYDOWN(key) CInput::GetInst()->KeyDown(key)
#define KEYPRESS(key) CInput::GetInst()->KeyPress(key)
#define KEYUP(key) CInput::GetInst()->KeyUp(key)
#define GETBUFFER CResourceManager::GetInst()->GetBuffer()
#define FIND_PATH(path) GET_SINGLE(CPathManager)->FindPath(path)
#define COLLISION(pColl)\
 switch (pColl->GetCollType()) {\
	case CT_RECT:\
		return CCollisionManager::GetInst()->IsCollided(this, (CCollisionRect*)pColl);\
	case CT_SPHERE:\
		return CCollisionManager::GetInst()->IsCollided(this, (CCollisionSphere*)pColl);\
	case CT_LINE:\
		return CCollisionManager::GetInst()->IsCollided(this, (CCollisionLine*)pColl);\
	case CT_POINT:\
		return CCollisionManager::GetInst()->IsCollided(this, (CCollisionPoint*)pColl);\
	case CT_PIXEL:\
		return CCollisionManager::GetInst()->IsCollided(this, (CCollisionPixel*)pColl);\
	}\
return false;
		
const float GRAVITY = 600.f;
