#pragma once
#include"Flag.h"
typedef struct _tagResolution {
    unsigned int iW;
    unsigned int iH;
	_tagResolution() {
		iW = 0;
		iH = 0;
	}
	_tagResolution(unsigned int w, unsigned int h) {
		iW = w;
		iH = h;
	}
	_tagResolution(const _tagResolution& rs) {
		iW = rs.iW;
		iH = rs.iH;
	}
}RESOLUTION;
typedef struct _tagFloatPosition {
	float x, y;
	_tagFloatPosition() :x(0.f), y(0.f) {}
	_tagFloatPosition(float _x, float _y) :x(_x), y(_y) {}
	_tagFloatPosition(const _tagFloatPosition& fpos) :x(fpos.x), y(fpos.y) {}
	_tagFloatPosition(const POINT& pt) :x((float)pt.x), y((float)pt.y) {}
	_tagFloatPosition(const RESOLUTION& rs) :x((float)rs.iW), y((float)rs.iH) {}

	_tagFloatPosition operator+(const _tagFloatPosition& fpos) {
		_tagFloatPosition pos(x + fpos.x, y + fpos.y);
		return pos;

	}
	_tagFloatPosition operator+(const POINT& fpos) {
		_tagFloatPosition pos(x + fpos.x, y + fpos.y);
		return pos;

	}
	_tagFloatPosition operator+(int n) {
		_tagFloatPosition pos(x + n, y + n);
		return pos;
	}
	_tagFloatPosition operator+(float n) {
		_tagFloatPosition pos(x + n, y + n);
		return pos;
	}
	_tagFloatPosition operator+=(const _tagFloatPosition& fpos) {
		*this = _tagFloatPosition(x + fpos.x, y + fpos.y);
		return *this;

	}
	_tagFloatPosition operator+=(const POINT& fpos) {
		*this = _tagFloatPosition(x + fpos.x, y + fpos.y);
		return *this;

	}
	_tagFloatPosition operator+=(int n) {
		*this = _tagFloatPosition(x + n, y + n);
		return *this;
	}
	_tagFloatPosition operator+=(float n) {
		*this = _tagFloatPosition(x + n, y + n);
		return *this;
	}

	_tagFloatPosition operator-(const _tagFloatPosition& fpos) {
		_tagFloatPosition pos(x - fpos.x, y - fpos.y);
		return pos;

	}
	_tagFloatPosition operator-(const POINT& fpos) {
		_tagFloatPosition pos(x - fpos.x, y - fpos.y);
		return pos;

	}
	_tagFloatPosition operator-(int n) {
		_tagFloatPosition pos(x - n, y - n);
		return pos;

	}
	_tagFloatPosition operator-(float n) {
		_tagFloatPosition pos(x - n, y - n);
		return pos;

	}
	_tagFloatPosition operator-=(const _tagFloatPosition& fpos) {
		*this = _tagFloatPosition(x - fpos.x, y - fpos.y);
		return *this;

	}
	_tagFloatPosition operator-=(const POINT& fpos) {
		*this = _tagFloatPosition(x - fpos.x, y - fpos.y);
		return *this;

	}
	_tagFloatPosition operator-=(int n) {
		*this = _tagFloatPosition(x - n, y - n);
		return *this;
	}
	_tagFloatPosition operator-=(float n) {
		*this = _tagFloatPosition(x - n, y - n);
		return *this;
	}

	_tagFloatPosition operator*(const _tagFloatPosition& fpos) {
		_tagFloatPosition pos(x * fpos.x, y * fpos.y);
		return pos;

	}
	_tagFloatPosition operator*(const POINT& fpos) {
		_tagFloatPosition pos(x * fpos.x, y * fpos.y);
		return pos;

	}
	_tagFloatPosition operator*(int n) const {
		_tagFloatPosition pos(x * n, y * n);
		return pos;
	}
	_tagFloatPosition operator*(float n) const {
		_tagFloatPosition pos(x * n, y * n);
		return pos;
	}
	_tagFloatPosition operator*=(const _tagFloatPosition& fpos) {
		*this = _tagFloatPosition(x * fpos.x, y * fpos.y);
		return *this;

	}
	_tagFloatPosition operator*=(const POINT& fpos) {
		*this = _tagFloatPosition(x * fpos.x, y * fpos.y);
		return *this;

	}
	_tagFloatPosition operator*=(int n) {
		*this = _tagFloatPosition(x * n, y * n);
		return *this;
	}
	_tagFloatPosition operator*=(float n) {
		*this = _tagFloatPosition(x * n, y * n);
		return *this;
	}

	_tagFloatPosition operator/(const _tagFloatPosition& fpos) {
		_tagFloatPosition pos(x / fpos.x, y / fpos.y);
		return pos;

	}
	_tagFloatPosition operator/(const POINT& fpos) {
		_tagFloatPosition pos(x / fpos.x, y / fpos.y);
		return pos;

	}
	_tagFloatPosition operator/(int n) {
		_tagFloatPosition pos(x / n, y / n);
		return pos;
	}
	_tagFloatPosition operator/(float n) {
		_tagFloatPosition pos(x / n, y / n);
		return pos;
	}
	_tagFloatPosition operator/=(const _tagFloatPosition& fpos) {
		*this = _tagFloatPosition(x / fpos.x, y / fpos.y);
		return *this;

	}
	_tagFloatPosition operator/=(const POINT& fpos) {
		*this = _tagFloatPosition(x / fpos.x, y / fpos.y);
		return *this;

	}
	_tagFloatPosition operator/=(int n) {
		*this = _tagFloatPosition(x / n, y / n);
		return *this;
	}
	_tagFloatPosition operator/=(float n) {
		*this = _tagFloatPosition(x / n, y / n);
		return *this;
	}


	_tagFloatPosition operator=(const _tagFloatPosition& fpos) {
		x = (float)fpos.x;
		y = (float)fpos.y;
		return *this;
	}
	_tagFloatPosition operator=(const POINT& fpos) {
		x = (float)fpos.x;
		y = (float)fpos.y;
		return *this;
	}
	bool operator<(const _tagFloatPosition& fpos) {
		return x < fpos.x;
	}
	bool operator>(const _tagFloatPosition& fpos) {
		return x > fpos.x;
	}
	bool operator<=(const _tagFloatPosition& fpos) {
		return x <= fpos.x;
	}
	bool operator>=(const _tagFloatPosition& fpos) {
		return x >= fpos.x;
	}
}FPOS, FSIZE;
typedef struct _tagFloatLine {
	FPOS begin;
	FPOS end;
	//생성자
	_tagFloatLine() :begin(0.f, 0.f), end(0.f, 0.f) {}
	_tagFloatLine(FPOS tBegin, FPOS tEnd) :begin(tBegin), end(tEnd) {}
	_tagFloatLine(const _tagFloatLine& fline) :begin(fline.begin), end(fline.end) {}
	//내장함수
	float Gradient() {
		FSIZE dist = end - begin;
		if (dist.x == 0)
			return 100000.f;
		return dist.y / dist.x;
	}
	float DistX() { return abs(end.x - begin.x); }
	float DistY() { return abs(end.y - begin.y); }
	float Dist() { return sqrt(pow(DistX(), 2) + pow(DistY(), 2)); }
	//연산자
	_tagFloatLine operator+(const _tagFloatLine& fLine) {
		_tagFloatLine line(begin + fLine.begin, end + fLine.end);
		return line;

	}
	_tagFloatLine operator+(int i) {
		_tagFloatLine line(begin + i, end + i);
		return line;
	}
	_tagFloatLine operator+(float f) {
		_tagFloatLine line(begin + f, end + f);
		return line;
	}
	_tagFloatLine operator+=(const _tagFloatLine& fLine) {
		*this = *this + fLine;
		return *this;

	}
	_tagFloatLine operator+=(int i) {
		*this = *this + i;
		return *this;
	}
	_tagFloatLine operator+=(float f) {
		*this = *this + f;
		return *this;
	}
	_tagFloatLine operator-(const _tagFloatLine& fLine) {
		_tagFloatLine line(begin - fLine.begin, end - fLine.end);
		return line;

	}
	_tagFloatLine operator-(int i) {
		_tagFloatLine line(begin - i, end - i);
		return line;
	}
	_tagFloatLine operator-(float f) {
		_tagFloatLine line(begin - f, end - f);
		return line;
	}
	_tagFloatLine operator-=(const _tagFloatLine& fLine) {
		*this = *this - fLine;
		return *this;

	}
	_tagFloatLine operator-=(int i) {
		*this = *this - i;
		return *this;
	}
	_tagFloatLine operator-=(float f) {
		*this = *this - f;
		return *this;
	}
	_tagFloatLine operator*(const _tagFloatLine& fLine) {
		_tagFloatLine line(begin * fLine.begin, end * fLine.end);
		return line;

	}
	_tagFloatLine operator*(int i) {
		_tagFloatLine line(begin * i, end * i);
		return line;
	}
	_tagFloatLine operator*(float f) {
		_tagFloatLine line(begin * f, end * f);
		return line;
	}
	_tagFloatLine operator*=(const _tagFloatLine& fLine) {
		*this = *this * fLine;
		return *this;

	}
	_tagFloatLine operator*=(int i) {
		*this = *this * i;
		return *this;
	}
	_tagFloatLine operator*=(float f) {
		*this = *this * f;
		return *this;
	}
	_tagFloatLine operator/(const _tagFloatLine& fLine) {
		_tagFloatLine line(begin / fLine.begin, end / fLine.end);
		return line;

	}
	_tagFloatLine operator/(int i) {
		_tagFloatLine line(begin / i, end / i);
		return line;
	}
	_tagFloatLine operator/(float f) {
		_tagFloatLine line(begin / f, end / f);
		return line;
	}
	_tagFloatLine operator/=(const _tagFloatLine& fLine) {
		*this = *this / fLine;
		return *this;

	}
	_tagFloatLine operator/=(int i) {
		*this = *this / i;
		return *this;
	}
	_tagFloatLine operator/=(float f) {
		*this = *this / f;
		return *this;
	}
	_tagFloatLine operator=(const _tagFloatLine& fLine) {
		begin = fLine.begin;
		end = fLine.end;
		return *this;
	}
}FLINE;
using PIXEL =
struct _tagPixel {
	unsigned char b;
	unsigned char g;
	unsigned char r;
	_tagPixel() :r(0), g(0), b(0) {}
	_tagPixel(int b,int g,int r) :r(r), g(g), b(b) {}
	bool operator==(const _tagPixel p) {
		return r==p.r&&g==p.g&&b==p.b;
	}
};
using PIXELROW =
struct _tagPixelRow {
	std::vector<PIXEL> row;
};

typedef struct _tagAnimationClip
{
	ANIMATION_OPTION eOption;
	class CTexture*	pTexture;
	float		fAnimationTime;
	float		fAnimationLimitTime;
	float		fAnimationFrameTime;
	float		fAnimationFrameLimitTime;
	int			iFrame;
	int			iLength;
	SIZE		tFrameSize;
}ANIMATIONCLIP, *pANIMATIONCLIP;