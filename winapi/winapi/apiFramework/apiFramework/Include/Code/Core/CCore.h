#pragma once
#include "Header/game.h"
#include"Manager/CSceneManager.h"
#include"Manager/CPathManager.h"
#include"Manager/CResourceManager.h"
#include"Manager/CTime.h"
#include"Manager/CTexture.h"
class CCore {
private:
    static CCore*	m_pInst;
    bool			m_bLoop;		//루프
	RESOLUTION		m_tRS;			//윈도우 크기
    HINSTANCE		m_hInst;		//인스턴스 핸들
    HWND			m_hWnd;			//윈도우 핸들
    HDC				m_hDC;			//DC핸들
	HDC				m_hBackDC;		//백버퍼DC핸들
	LPCWSTR			m_wstrClassName;//클래스 이름
	class CMouse*	m_pMouse;		//마우스

private:
    CCore();
    ~CCore();
private:
    ATOM	MyRegisterClass();//윈도우 등록
    bool	create();//윈도우 생성
    void	Logic();//게임 로직
    void	Update(float fDeltaTime);
    void	LateUpdate(float fDeltaTime);
    void	Collision(float fDeltaTime);
    void	Render(float fDeltaTime);
    
public:
    static CCore*	GetInst();
    static void		DestroyInst();

	bool GetLoop() { return m_bLoop; }
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
public:
	void		DestroyGame() {
		DestroyWindow(m_hWnd);
	}
	RESOLUTION	GetResolution() const { return m_tRS; }
	HWND		GetWindowHandle() const { return m_hWnd; }
    bool		init(HINSTANCE hInst);//초기화
    int			Run();//실행
};