#pragma once
#include "CCore.h"
#include"Manager/CTime.h"
#include"Manager/CSceneManager.h"
#include"Manager/CCamera.h"
#include"Manager/CInput.h"
#include"Manager/CCollisionManager.h"
#include"../Obj/CGObj.h"
#include"../Obj/UI/CMouse.h"
#include"../Obj/Static/CTile.h"

CCore* CCore::m_pInst = nullptr;
CCore::CCore() :
	m_wstrClassName(L"MyApi"),
	m_bLoop(false),
	m_tRS(),
	m_hInst(),
	m_hWnd(),
	m_hDC(),
	m_hBackDC(),
	m_pMouse(nullptr)
{

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#ifdef _DEBUG
	AllocConsole();
#endif
	//_CrtSetBreakAlloc(3261);
}

CCore::~CCore()
{
	SAFE_RELEASE(m_pMouse);
	CGObj::EraseAllProto();
	CGObj::EraseAllObj();
	DESTROY_SINGLE(CCamera);
	DESTROY_SINGLE(CInput);
    DESTROY_SINGLE(CSceneManager);
	DESTROY_SINGLE(CCollisionManager);
	DESTROY_SINGLE(CPathManager);
	DESTROY_SINGLE(CResourceManager);
    DESTROY_SINGLE(CTime);
	ReleaseDC(m_hWnd, m_hDC);
#ifdef _DEBUG
	FreeConsole();
#endif
}

ATOM CCore::MyRegisterClass()
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = m_hInst;
    wcex.hIcon = LoadIcon(m_hInst, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = nullptr;
    wcex.lpszClassName = m_wstrClassName;
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));
    return RegisterClassExW(&wcex);
    return ATOM();
}
LRESULT CCore::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)

{

    switch (message)
    {
	case WM_CREATE:
		break;
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);
        EndPaint(hWnd, &ps);
    }
    break;
    case WM_KEYDOWN:
        if (wParam == VK_ESCAPE)
            DestroyWindow(hWnd);
        break;
    case WM_DESTROY:
        GetInst()->m_bLoop = false;
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProcW(hWnd, message, wParam, lParam);
    }
    return (int)wParam;
}

CCore * CCore::GetInst()
{
    if (!m_pInst)
        m_pInst = new CCore;
    return m_pInst;
}
void CCore::DestroyInst()
{
    SAFE_DELETE(m_pInst);
}
bool CCore::create()
{
	wchar_t str[] = L"nanana";
    m_hWnd = CreateWindowW(m_wstrClassName,str, WS_OVERLAPPEDWINDOW,
        100, 50, (int)m_tRS.iW, (int)m_tRS.iH, nullptr, nullptr, m_hInst, nullptr);

    if (!m_hWnd)
    {
        return FALSE;
    }
	SetWindowTextW(m_hWnd, L"123");
    RECT rc = { 0,0,(int)m_tRS.iW,(int)m_tRS.iH};
    AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, false);
    SetWindowPos(m_hWnd, HWND_TOPMOST, 0, 0, rc.right - rc.left, rc.bottom - rc.top, SWP_NOMOVE | SWP_NOZORDER);
    ShowWindow(m_hWnd, SW_SHOW);
    UpdateWindow(m_hWnd);

    return TRUE;
}
bool CCore::init(HINSTANCE hInst)
{
    m_bLoop = true;
    m_hInst = hInst;
	MyRegisterClass();
	m_tRS.iW = 1280;
    m_tRS.iH = 720;

	if (!create())
		return false;
    m_hDC=GetDC(m_hWnd);
	if (!GET_SINGLE(CPathManager)->Init())
		return false;
	if (!GET_SINGLE(CResourceManager)->Init(m_hInst,m_hDC))
		return false;
    if (!GET_SINGLE(CSceneManager)->Init())
        return false;
	if (!GET_SINGLE(CCollisionManager)->Init())
		return false;
	if (!GET_SINGLE(CCamera)->Init(FPOS(0.f,0.f),m_tRS,RESOLUTION(1800,899)))
		return false;
	if (!GET_SINGLE(CInput)->Init(m_hWnd))
		return false;
    if (!GET_SINGLE(CTime)->Init(m_hWnd))
        return false;
	m_pMouse = CGObj::CreateObj<CMouse>("Mouse");
	m_pMouse->AddRef();
	GET_SINGLE(CInput)->SetMouse(m_pMouse);
	ShowCursor(false);

    return true;
}
void CCore::Logic() {
	GET_SINGLE(CTime)->Update();
    float fDeltaTime = GET_SINGLE(CTime)->GetDeltaTime();
    Update(fDeltaTime);
    Collision(fDeltaTime);
    LateUpdate(fDeltaTime);
    Render(fDeltaTime);
}
void CCore::Update(float fDeltaTime)
{
	GET_SINGLE(CInput)->Update(fDeltaTime);
	GET_SINGLE(CSceneManager)->Update(fDeltaTime);
	GET_SINGLE(CCamera)->Update(fDeltaTime);
	m_pMouse->Update(fDeltaTime);
}
void CCore::LateUpdate(float fDeltaTime)
{
	GET_SINGLE(CSceneManager)->LateUpdate(fDeltaTime);
	m_pMouse->LateUpdate(fDeltaTime);
}
void CCore::Collision(float fDeltaTime)
{
	GET_SINGLE(CCollisionManager)->Collision(fDeltaTime);
}
void CCore::Render(float fDeltaTime)
{
	CTexture* pBackBuffer = GET_SINGLE(CResourceManager)->GetBackBuffer();
	Rectangle(pBackBuffer->GetDC(), 0, 0, m_tRS.iW, m_tRS.iH);
	GET_SINGLE(CSceneManager)->Render(pBackBuffer->GetDC(),fDeltaTime);
	m_pMouse->Render(pBackBuffer->GetDC(), fDeltaTime);
	BitBlt(m_hDC, 0, 0, m_tRS.iW, m_tRS.iH, pBackBuffer->GetDC(), 0, 0, SRCCOPY);
}
int CCore::Run()
{
    MSG msg;
    while (m_bLoop)
    {
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else {
            Logic();
        }
    }
    ReleaseDC(m_hWnd, m_hDC);
    return 0;
}
