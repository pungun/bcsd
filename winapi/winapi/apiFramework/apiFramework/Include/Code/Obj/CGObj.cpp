#include "CGObj.h"
#include"../Collision/CCollision.h"
#include"../Core/CCore.h"
list<CGObj*>  CGObj::m_ObjList;
unordered_map<string, CGObj*> CGObj::m_PrototypeMap;

CGObj::CGObj()
    :CReleaseable(),
	m_strTag(""),
	m_tPos(),
	m_tSize(),
	m_tPivot(),
	m_pAnimation(nullptr),
	m_pTexture(nullptr),
	m_pScene(nullptr),
	m_pLayer(nullptr)
{
}

CGObj::CGObj(const CGObj & obj):
	m_pTexture(nullptr)
{
	*this = obj;
	if (obj.m_pAnimation) {
		m_pAnimation->AddRef();
	}
	if (obj.m_pTexture) {
		m_pTexture->AddRef();
	}
	m_CollisionList.clear();
	for (auto collision : obj.m_CollisionList)
		m_CollisionList.push_back(collision->Clone());
}


CGObj::~CGObj()
{
	if(GET_SINGLE(CCore)->GetLoop())
		for (auto e : m_DeleteEventList)
			e();
	SAFE_RELEASE(m_pAnimation);
	SAFE_RELEASE(m_pTexture);
	for (auto &coll : m_CollisionList) {
		GET_SINGLE(CCollisionManager)->RemoveCollision(coll);
		SAFE_RELEASE(coll);
	}
	m_CollisionList.clear();
}


CGObj * CGObj::FindObject(const string & strTag)
{
	for (auto obj : m_ObjList)
		if (obj->GetTag() == strTag)
			return obj;
	return nullptr;
}

CGObj * CGObj::FindPrototype(const string & strTag)
{
	for (auto obj : m_PrototypeMap)
		if (obj.first == strTag)
			return obj.second;
	return nullptr;
}

void CGObj::EraseObj(CGObj * pObj)
{
	auto iter = m_ObjList.begin();
	while (iter != m_ObjList.end()) {
		if ((*iter) == pObj) {
			SAFE_RELEASE((*iter));
			m_ObjList.erase(iter);
			return;
		}
		++iter;
	}
}

void CGObj::EraseObj(const string & strTag)
{
	auto iter = m_ObjList.begin();
	auto iterEnd = m_ObjList.end();
	while (iter != iterEnd) {
		if ((*iter)->GetTag() == strTag) {
			SAFE_RELEASE((*iter));
			m_ObjList.erase(iter);
			return;
		}
		++iter;
	}
}

void CGObj::EraseAllObj()
{
	auto iter = m_ObjList.begin();
	auto iterEnd = m_ObjList.end();
	while (iter != iterEnd){
			SAFE_RELEASE((*iter));
			iter=m_ObjList.erase(iter);
		}
}


void CGObj::EraseProto(const string & strTag)
{
	auto iter = m_PrototypeMap.find(strTag);
	if (!iter->second)
		return;
	SAFE_RELEASE(iter->second);
	m_PrototypeMap.erase(iter);
}
	

void CGObj::EraseAllProto()
{
	SAFE_RELEASE_MAP(m_PrototypeMap);
}

bool CGObj::Init()
{
    return true;
}

int CGObj::Update(float fDeltaTime)
{
	for (auto e : m_UpdateEventList)
		e(fDeltaTime);
	auto iter= m_CollisionList.begin();
	while (iter != m_CollisionList.end()) {
		if (!(*iter)->GetLife()) {
			SAFE_RELEASE((*iter));
			iter = m_CollisionList.erase(iter);
			continue;
		}
		if ((*iter)->GetEnable())
			(*iter)->Update(fDeltaTime);
		++iter;
	}
	if (m_pAnimation) {
		if (!m_pAnimation->GetLife()) {
			SAFE_RELEASE(m_pAnimation);
		}
		else if (m_pAnimation - GetEnable()) {
			m_pAnimation->Update(fDeltaTime);
		}
	}
		
	return 0;
}

int CGObj::LateUpdate(float fDeltaTime)
{
	auto iter = m_CollisionList.begin();
	while (iter != m_CollisionList.end()) {
		if (!(*iter)->GetLife()) {
			SAFE_RELEASE((*iter));
			iter = m_CollisionList.erase(iter);
			continue;
		}
		if ((*iter)->GetEnable())
			(*iter)->LateUpdate(fDeltaTime);
		++iter;
	}
	return 0;
}

void CGObj::Render(HDC hDC, float fDeltaTime)
{
	if (Right() < 0 || Bottom() < 0)
		return;
	if (Left() > (int)GET_SINGLE(CCamera)->GetCllientRS().iW || Top() > (int)GET_SINGLE(CCamera)->GetCllientRS().iH)
		return;
	if (m_pAnimation) {
		TransparentBlt(hDC, Left(), Top(), m_pAnimation->GetWidth(), m_pAnimation->GetHeight(), m_pAnimation->GetDC(), m_pAnimation->GetXStart(), 0, m_pAnimation->GetWidth(), m_pAnimation->GetHeight(), TRANS_COR);
	}
	else if (m_pTexture) {
		TransparentBlt(hDC, Left(), Top(), (int)m_tSize.x, (int)m_tSize.y, m_pTexture->GetDC(), 0, 0, (int)m_tSize.x, (int)m_tSize.y,TRANS_COR);
	}
	//�浹ü ������
	/*
	auto iter = m_CollisionList.begin();
	while (iter != m_CollisionList.end()) {
		if (!(*iter)->GetLife()) {
			SAFE_RELEASE((*iter));
			iter = m_CollisionList.erase(iter);
			continue;
		}
		if ((*iter)->GetEnable())
			(*iter)->Render(hDC,fDeltaTime);
		++iter;
	}*/
}

void CGObj::AddObjList()
{
	AddRef();
	m_ObjList.push_back(this);
}

void CGObj::AddProtoMap()
{
	m_PrototypeMap.insert(make_pair(m_strTag, this));
	AddRef();
}

CGObj * CGObj::CloneObj(const string & typeKey, const string & strTag, CLayer * pLayer)
{

	auto pPrototype = FindPrototype(typeKey);
	if (!pPrototype)
		return nullptr;
	auto pObj = pPrototype->Clone();
	pObj->SetTag(strTag);
	pObj->AddObjList();
	pObj->Release();
	if (pLayer)
		pLayer->AddObject(pObj);
	return pObj;
}


