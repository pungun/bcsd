#pragma once
#include"../Core/Manager/CReleaseable.h"
#include"../Screen/CLayer.h"
#include"../Core/Manager/CSceneManager.h"
#include"../Core/Manager/CTexture.h"
#include"../Core/Manager/CCamera.h"
#include"../Core/Manager/CCollisionManager.h"
#include"../Core/Manager/CResourceManager.h"
#include"../Animation/CAnimation.h"
using OnDelete = function<void(void)>;
using OnUpdate = function<void(float)>;
class CGObj
	:public CReleaseable
{
private:
	//���� ����
	static list<CGObj*> m_ObjList;
	static unordered_map<string, CGObj*> m_PrototypeMap;
protected:
	//��� ����
    friend class CLayer;
    string			m_strTag;
    FPOS			m_tPos;
    FSIZE			m_tSize;
	FPOS			m_tPivot;
	CAnimation*		m_pAnimation;
	CTexture*		m_pTexture;
	CScene*			m_pScene;
	CLayer*			m_pLayer;
protected:
	//��� STL
	list<class CCollision*>	m_CollisionList;
	list<OnUpdate>			m_UpdateEventList;
	list<OnDelete>			m_DeleteEventList;
protected:
	//������ �Ҹ���
    CGObj();
    CGObj(const CGObj& obj);
    virtual ~CGObj();
private:
	//private �Լ�
	static CGObj* FindPrototype(const string& strTag);
public:
	static CGObj* FindObject(const string& strTag);
	static CGObj* CloneObj(const string& typeKey, const string& strTag, CLayer* pLayer = nullptr);
	static void EraseObj(CGObj* pObj);
	static void EraseObj(const string& strTag);
	static void EraseAllObj();
	static void EraseProto(const string& strTag);
	static void EraseAllProto();
public:
    virtual bool Init()=0;
	virtual CGObj* Clone() = 0;
    virtual int Update(float fDeltaTime);
    virtual int LateUpdate(float fDeltaTime);
    virtual void Render(HDC hDC, float fDeltaTime);
	void AddUpdateEvent(OnUpdate e) { m_UpdateEventList.push_back(e); }
	void AddDeleteEvent(OnDelete e) { m_DeleteEventList.push_back(e); }
	void EraseAllUpdateEvent() { m_UpdateEventList.clear(); }
	void EraseAllDeleteEvent() { m_DeleteEventList.clear(); }
	void AddObjList();
	void AddProtoMap();
public:
	//getter,setter
	int			Left() {
		return (int)(GetLeftTop().x-GET_SINGLE(CCamera)->GetLeftTop().x);
	}
	int			Top() {
		return (int)(GetLeftTop().y - GET_SINGLE(CCamera)->GetLeftTop().y);
	}
	int			Right() {
		return Left()+(int)m_tSize.x;
	}
	int			Bottom() {
		return Top() + (int)m_tSize.y;
	}
    string		GetTag()const {
 		return m_strTag;
	}
    void		SetTag(const string& strTag) {
		m_strTag = strTag;
	}
    FPOS		GetPos() const {
		return m_tPos;
	}
    void		SetPos(const FPOS& tPos) {
		m_tPos = tPos;
	}
    void		SetPos(const POINT& tPos) {
		m_tPos = FPOS(tPos);
	}
    void		SetPos(float x, float y) {
		m_tPos.x = x;
		m_tPos.y = y;
	}
	FPOS		GetLeftTop()const {
		return (FPOS)m_tPos - (FPOS)m_tSize * m_tPivot;
	}		
	void		SetLTPos(const FPOS& tPos){
		m_tPos = (FPOS)tPos + m_tPivot * m_tSize;
	}
	void		SetLTPos(float x, float y){
		SetLTPos(FPOS(x, y));
	}
	FPOS		GetPivot() const {
		return m_tPivot;
	}
	void		SetPivot(const FPOS& tPivot) {
		m_tPivot = tPivot;
	}
	void		SetPivot(float x,float y) {
		SetPivot(FPOS(x, y));
	}
	FPOS		GetPivotPos(FPOS tPivot)const {
		return GetLeftTop()+(FPOS)m_tSize*tPivot;
	}
	FPOS		GetPivotPos(float x,float y)const {
		return GetLeftTop() + (FPOS)m_tSize*FPOS(x,y);
	}
    FSIZE		GetSize() const {
		return m_tSize;
	}
    void		SetSize(const FSIZE& tSize) {
		m_tSize = tSize;
	}
    void		SetSize(float x, float y) {
		m_tSize.x = x;
		m_tSize.y = y;
	}
	class		CScene* GetScene() {
		return m_pScene;
	}
	void		SetScene(class CScene* pScene) {
		m_pScene = pScene;
	}
	CLayer*		GetLayer() {
		return m_pLayer;
	}
	void		SetLayer(CLayer* pLayer) {
		m_pLayer = pLayer;
	}
	CTexture*	GetTexture() {
		return m_pTexture;
	}
	void		SetTexture(CTexture* pTexture) {
		SAFE_RELEASE(m_pTexture);
		if(pTexture)
		m_pTexture = pTexture;
		pTexture->AddRef();
	}
	CAnimation*	GetAnimation() {
		return m_pAnimation;
	}
	void		SetAnimaton(CAnimation* pAnimation) {
		SAFE_RELEASE(m_pAnimation);
		if (pAnimation) {
			m_pAnimation = pAnimation;
			m_pAnimation->AddRef();
		}
	}
	void		SetTexture(const string& strKey, const wchar_t* pFileName = NULL,
						   const string& strPathKey = PATH_TEXTURE) {
		SAFE_RELEASE(m_pTexture);
		m_pTexture = GET_SINGLE(CResourceManager)->LoadTexture(strKey, pFileName, strPathKey);
		if(m_pTexture)
			m_pTexture->AddRef();
	}
public:
	//���ø� �Լ�
	template<class T>
	T* AddCollision(const string& strTag) {
		T* pCollision = new T;
		pCollision->SetObj(this);
		if (!pCollision->Init()) {
			SAFE_RELEASE(pCollision);
			return nullptr;
		}
		pCollision->SetTag(strTag);
		m_CollisionList.push_back(pCollision);
		pCollision->AddRef();
		GET_SINGLE(CCollisionManager)->AddCollision(pCollision);
		return pCollision;
	}
	template<class T>
	static T* CreateObj(const string& strTag, CLayer* pLayer = nullptr) {
		T* pObj = new T;
		if (!pObj->Init()) {
			SAFE_RELEASE(pObj);
			return nullptr;
		}
		pObj->SetTag(strTag);
		pObj->AddObjList();
		if (pLayer)
			pLayer->AddObject(pObj);
		return pObj;
	}
	template<class T>
	static T* CreatePrototyle(const string& strTag) {
		T* pObj = new T;
		if (!pObj->Init()) {
			SAFE_RELEASE(pObj);
			return nullptr;
		}
		pObj->SetTag(strTag);
		pObj->AddProtoMap();
		return pObj;
	}
};

