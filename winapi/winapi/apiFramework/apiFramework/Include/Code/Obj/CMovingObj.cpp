#include "CMovingObj.h"
#include "../Core/Manager/CResourceManager.h"

CMovingObj::CMovingObj()
	:CGObj(),
	m_fAngle(0.f),
	m_fSpeed(0.f),
	m_bIsPhysics(false),
	m_fFallSpeed(0),
	m_fDir(0)
{
}

CMovingObj::CMovingObj(const CMovingObj & obj)
    :CGObj(obj)
{
	m_fAngle = obj.m_fAngle;
	m_fSpeed = obj.m_fSpeed;
	m_bIsPhysics = obj.m_bIsPhysics;
	m_fFallSpeed = obj.m_fFallSpeed;
	m_fDir = obj.m_fDir;
}


CMovingObj::~CMovingObj()
{
}



void CMovingObj::Move(float x, float y)
{
	m_tPos.x += x;
	m_tPos.y += y;
}

void CMovingObj::Move(float x, float y, float fDeltaTime)
{
	m_tPos.x += x*fDeltaTime;
	m_tPos.y += y*fDeltaTime;
}

void CMovingObj::Move(const FPOS & tMove)
{
	m_tPos = m_tPos + tMove;
}

void CMovingObj::Move(const FPOS & tMove, float fDeltaTime)
{
	m_tPos = m_tPos + tMove*fDeltaTime;
}


void CMovingObj::MoveX(float x)
{
	m_tPos.x += x;
}

void CMovingObj::MoveX(float x, float fDeltaTime)
{
	m_tPos.x += x* fDeltaTime;
}

void CMovingObj::MoveXSpeed(float fDeltaTime)
{
	m_tPos.x += m_fSpeed * fDeltaTime;
}

void CMovingObj::MoveY(float y)
{
	m_tPos.y += y;
}

void CMovingObj::MoveY(float y, float fDeltaTime)
{
	m_tPos.y += y* fDeltaTime;
}

void CMovingObj::MoveYSpeed(float fDeltaTime)
{
	m_tPos.y += m_fSpeed * fDeltaTime;
}


float CMovingObj::MoveAngle()
{
	m_tPos.x += cosf(m_fAngle)*m_fSpeed;
	m_tPos.y += sinf(m_fAngle)*m_fSpeed;
	return m_fSpeed;
}

float CMovingObj::MoveAngle( float fTime)
{
	float dist = m_fSpeed * fTime;
	m_tPos.x += cosf(m_fAngle)*dist;
	m_tPos.y += sinf(m_fAngle)*dist;
	return dist;
}

bool CMovingObj::Init()
{
	return true;
}

int CMovingObj::Update(float fDeltaTime)
{
	CGObj::Update(fDeltaTime);
	if (m_bIsPhysics) {
		m_fFallSpeed += fDeltaTime * GRAVITY;
		MoveY(m_fFallSpeed, fDeltaTime);
	}
	return 0;
}

void CMovingObj::Render(HDC hDC, float fDeltaTime)
{
	auto pAnimation = GetAnimation();
	if(pAnimation) {
		auto buffer = GET_SINGLE(CResourceManager)->GetBuffer();
		StretchBlt(buffer->GetDC(), (GetDir()==1.f)?0:pAnimation->GetWidth()-1, 0, ((GetDir() == 1.f) ? 1 : -1)*pAnimation->GetWidth(),
			pAnimation->GetHeight(), pAnimation->GetDC(), pAnimation->GetXStart(), 0, pAnimation->GetWidth(), pAnimation->GetHeight(), SRCCOPY);
		TransparentBlt(hDC, Left(), Top(), pAnimation->GetWidth(), pAnimation->GetHeight(), buffer->GetDC(),0, 0, pAnimation->GetWidth(), pAnimation->GetHeight(), TRANS_COR);
	}
	else if (m_pTexture) {
	TransparentBlt(hDC, Left(), Top(), (int)m_tSize.x, (int)m_tSize.y, m_pTexture->GetDC(), 0, 0, (int)m_tSize.x, (int)m_tSize.y, TRANS_COR);
	}
	//�浹ü ������
	/*
	auto iter = m_CollisionList.begin();
	while (iter != m_CollisionList.end()) {
		if (!(*iter)->GetLife()) {
			SAFE_RELEASE((*iter));
			iter = m_CollisionList.erase(iter);
			continue;
		}
		if ((*iter)->GetEnable())
			(*iter)->Render(hDC,fDeltaTime);
		++iter;
	}*/
}



