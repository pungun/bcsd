#pragma once
#include "CGObj.h"
class CStaticObj :
	public CGObj
{
protected:
	CStaticObj();
	CStaticObj(const CStaticObj& obj);
	virtual ~CStaticObj();

public:
	virtual bool	Init() = 0;
	virtual int		Update(float fDeltaTime);
	virtual int		LateUpdate(float fDeltaTime);
	virtual void	Render(HDC hDC, float fDeltaTime);
	virtual CStaticObj*	Clone() = 0;
};

