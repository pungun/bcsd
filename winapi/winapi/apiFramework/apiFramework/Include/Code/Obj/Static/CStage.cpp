#include "CStage.h"
#include"../../Core/CCore.h"


CStage::CStage()
{
}

CStage::CStage(const CStage & stage) :
	CStaticObj(stage)
{
}


CStage::~CStage()
{
}

bool CStage::Init()
{
	SetTexture("Stage1", L"stage1.bmp");
	SetPos(0.f, 0.f);
	SetSize((float)GetTexture()->GetInfo().bmWidth, (float)GetTexture()->GetInfo().bmHeight);
	SetPivot(FPOS(0.f, 0.f));


	return true;
}


int CStage::Update(float fDeltaTime)
{
	CStaticObj::Update(fDeltaTime);
	return 0;
}

int CStage::LateUpdate(float fDeltaTime)
{
	CStaticObj::LateUpdate(fDeltaTime);
	return 0;
}


void CStage::Render(HDC hDC, float fDeltaTime)
{

	if (m_pTexture)
	{
		FPOS tCamPos= GET_SINGLE(CCamera)->GetLeftTop();
		BitBlt(hDC, 0, 0,
			GET_RES.iW, GET_RES.iH,
			m_pTexture->GetDC(), (int)tCamPos.x, (int)tCamPos.y, SRCCOPY);
	}
	float FPS = GET_SINGLE(CTime)->GetFPS();
	char buffer[1024] = {};
	string fps;
	_itoa_s((int)FPS, buffer, 10);
	fps = buffer;
	TextOutA(hDC, 10, 10, fps.c_str(), fps.size());
}

CStage * CStage::Clone()
{
	return new CStage(*this);
}
