#pragma once
#include "../CStaticObj.h"
class CStage :
	public CStaticObj
{
private:
	friend class CGObj;
	friend class CScene;

	CStage();
	CStage(const CStage& stage);
	~CStage();

public:
	virtual bool	Init();
	virtual int		Update(float fDeltaTime);
	virtual int		LateUpdate(float fDeltaTime);
	virtual void	Render(HDC hDC, float fDeltaTime);
	virtual CStage*	Clone();
};