#pragma once
#include "../CMonster.h"
class CMinion :
	public CMonster
{
private:
	float	m_fReload;
	float	m_fReloadTime;
	float	 m_fDir;
	int		 m_iHP;
	int m_iBulletAmount;
	int m_iMaxBullet;
private:
	void Fire();
public:
	virtual int Update(float fDeltaTime);
	void Render(HDC hDC, float fDeltaTime);
public:
	CMinion();
	CMinion(const CMinion& obj);
	CMinion* Clone();
	~CMinion();
	bool Init();
public:
	int GetHP() { 
		return m_iHP;
	}
	void SetHP(int hp) { m_iHP = hp; }
	void ReviseHP(int value) { m_iHP += value; }
	void DeleteBullet() { --m_iBulletAmount; }

};

