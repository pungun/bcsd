#include "CMinion.h"
#include "../Other/CBullet.h"
#include"../../../Collision/CCollisionRect.h"
#include"../../../Collision/CCollisionSphere.h"
#include"../CPlayer.h"


void CMinion::Fire()
{
	if (m_iBulletAmount >= m_iMaxBullet)
		return;
	++m_iBulletAmount;
	CBullet* pBullet = (CBullet*)CGObj::CloneObj("Bullet", "MinionBullet", m_pLayer);
	FPOS p=GetPos();
	CGObj* pPlayer = FindObject("Player");
	pBullet->SetPos(p.x,p.y);
	float dx = (pPlayer)?pPlayer->GetPivotPos(0.5f,0.5f).x  - p.x:0;
	float dy = (pPlayer) ? pPlayer->GetPivotPos(0.5f,0.5f).y  - p.y:0;
	pBullet->SetAngle(atan2(dy, dx));
	pBullet->SetLimitDist(3000.f);
	pBullet->SetSpeed(500.f);
	pBullet->AddDeleteEvent([this]() {
		DeleteBullet();
	});
	auto pColl=pBullet->AddCollision<CCollisionSphere>("MinionBullet");
	pColl->SetPos(pBullet->GetLeftTop());
	pColl->SetRadius(pBullet->GetSize().x / 2);
	pColl->AddReact("Player");
	pColl->AddEvent(
		[=](CCollision* pColl)->void {
		CPlayer* pPlayer = (CPlayer*)pColl->GetObj();
		if (!pPlayer->GetLife())
			int i = 0;
		if (pBullet->GetEnable())
			pPlayer->ReviseHP(-(int)pBullet->GetPower());
		pBullet->Die();
		}
	);
}

int CMinion::Update(float fDeltaTime)
{
	CMonster::Update(fDeltaTime);
	if (GetHP() <= 0)
		Die();
	MoveYSpeed(fDeltaTime*m_fDir);
	if (GetLeftTop().y <0) {
		SetLTPos(GetLeftTop().x, 0);
		m_fDir = 1.f;
	}
	if (GetPivotPos(1,1).y>= (int)GET_WORLD_RS.iH) {
		SetLTPos(GetLeftTop().x,(float)GET_WORLD_RS.iH - GetSize().y);
		m_fDir = -1.f;
	}
	for (auto coll : m_CollisionList)
		if (coll->GetEnable())
			coll->Update(fDeltaTime);
	m_fReload += fDeltaTime;
	if (m_fReload >= m_fReloadTime) {
		m_fReload = 0.f;
		Fire();
	}
	return 0;
}

void CMinion::Render(HDC hDC, float fDeltaTime)
{
	int iHP = GetHP();
	char buffer[1024] = {};
	string hp;
	_itoa_s(iHP, buffer, 10);
	hp = buffer;
	Rectangle(hDC, Left(), Top(), Right(), Bottom());
	TextOutA(hDC, Left() + 10, Top() - 15, hp.c_str(), hp.size());
}

CMinion::CMinion()
	:CMonster(),
	m_fReload(0.f),
	m_fReloadTime(0.f),
	m_fDir(0.f),
	m_iHP(0),
	m_iBulletAmount(0),
	m_iMaxBullet(0)
{
}

CMinion::CMinion(const CMinion & obj)
	:CMonster(obj)
{
	m_fReload = obj.m_fReload;
	m_fReloadTime = obj.m_fReloadTime;
	m_fDir = obj.m_fDir;
	m_iHP = obj.m_iHP;
	m_iBulletAmount = obj.m_iBulletAmount;
	m_iMaxBullet = obj.m_iMaxBullet;
}

CMinion * CMinion::Clone()
{
	return new CMinion(*this);
}


CMinion::~CMinion()
{
}

bool CMinion::Init()
{
	if(!CMonster::Init())
		return false;
	SetPos(FPOS(1100.f, 0.f));
	SetSize(FSIZE(80.f, 80.f));
	SetSpeed(300.f);
	SetPivot(0.f, 0.5f);
	SetTag( "Minion");
	SetPhysics(false);
	m_fReload = 0.f;
	m_fReloadTime = 1.f;
	m_fDir = 1.f;
	m_iMaxBullet = 500;
	auto pColl = AddCollision<CCollisionRect>("MinionBody");
	pColl->SetRect(GetLeftTop(), FSIZE(50,50));
	pColl->AddReact("Stage");
	pColl->AddEvent([this](CCollision* pColl) {
		m_fDir = -1.f;
	});
	return true;
}
