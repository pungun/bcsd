#pragma once
#include "../CMovingObj.h"
class CMonster :
	public CMovingObj
{
public:
	CMonster();
	CMonster(const CMonster& obj);
	virtual CMonster* Clone();
	virtual ~CMonster();
	virtual bool Init();
	int Update(float fDeltaTime);
	void Render(HDC hDC, float fDeltaTime);
};

