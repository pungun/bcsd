#pragma once
#include "../../CMovingObj.h"
class CBullet :
	public CMovingObj
{
private:
	float m_fDist;
	float m_fLimitDist;
	float m_fPower;
public:
	CBullet();
	CBullet(const CBullet& bullet);
	~CBullet();
public:
	bool Init();
	int Update(float fDeltaTime);
	void Render(HDC hDC, float fDeltaTime);
	CBullet* Clone();
public:
	void SetLimitDist(float limitDist) {
		m_fLimitDist = limitDist;
	}
	float GetLimitDist() {
		return m_fLimitDist;
	}
	void SetPower(float power) {
		m_fPower = power;
	}
	float GetPower() {
		return m_fPower;
	}
	float GetDist() {
		return m_fDist;
	}
};

