#include "CPlayer.h"
#include"Other/CBullet.h"
#include"../../Core/Manager/CInput.h"
#include"../../Collision/CCollisionSphere.h"
#include"../../Collision/CCollisionRect.h"
#include"Monster/CMinion.h"


CPlayer::CPlayer() :
	m_fReload(0.f),
	m_fReloadTime(0.f),
	m_iHP(0),
	m_iPower(0),
	m_iBulletAmount(0),
	m_iMaxBullet(0),
	m_bJump(true),
	m_fJumpSpeed(0.f)

{
}

CPlayer::CPlayer(const CPlayer & obj)
    :CMovingObj(obj)
{
	m_fReload = obj.m_fReload;
	m_fReloadTime = obj.m_fReloadTime;
	m_iHP = obj.m_iHP;
	m_iPower = obj.m_iPower;
	m_iBulletAmount = obj.m_iBulletAmount;
	m_iMaxBullet = obj.m_iMaxBullet;
	m_bJump = obj.m_bJump;
	m_fJumpSpeed = obj.m_fJumpSpeed;
}


CPlayer::~CPlayer()
{
}

void CPlayer::Fire()
{
	if (m_fReload != 0||m_iBulletAmount>=m_iMaxBullet)
		return;
	++m_iBulletAmount;
 	CBullet* pBullet = (CBullet*)CGObj::CloneObj("Bullet", "PlayerBullet", m_pLayer);
	pBullet->SetPos(GetPivotPos(FPOS((1.f+ GetDir())/2,0.5f)));
	pBullet->SetAngle(0.f);
	pBullet->SetLimitDist(500.f);
	pBullet->SetSpeed(GetDir()*500.f);
	pBullet->AddDeleteEvent([this]() {
		if (this)
   			DeleteBullet();
	});
	auto pColl = pBullet->AddCollision<CCollisionSphere>("PlayerBullet");
	pColl->SetPos(pBullet->GetLeftTop());
	pColl->SetRadius(pBullet->GetSize().x / 2);
	pColl->AddReact("Minion");
	pColl->AddEvent(
		[=](CCollision* pColl){
		CMinion* pMinion = (CMinion*)pColl->GetObj();
		if (pBullet->GetEnable())
			pMinion->ReviseHP(-(int)pBullet->GetPower());
		pBullet->Die();
	}
	);
}

void CPlayer::Jump()
{
	SetJump(true);
	GetAnimation()->ChangeClip("PlayerJump");
}

bool CPlayer::Init()
{
	if (!CMovingObj::Init())
		return false;
    SetPos( FPOS(200.f, 400.f));
    
	SetHP(1000);
	SetPower(10);
	SetSpeed(300.f);
    SetTag("Player");
	SetPhysics(true);
	SetJumpSpeed(-450.f);
	SetJump(false);
	m_fReload = 0.f;
	m_fReloadTime = 0.01f;
	m_iMaxBullet = 500;
	SetPivot(FPOS(0.5f, 1.f));
	SetSize( FSIZE(36.f,38.f));
	auto pColl=AddCollision<CCollisionRect>("PlayerBody");
	pColl->SetSize(GetSize());
	pColl->AddReact("Stage");
	pColl->AddEvent([this](CCollision* pColl) {
		SetPhysics(false);
		SetFallSpeed(0.f);
		if (GetJump()) {
			GetAnimation()->ChangeDefaultClip();
			SetJump(false);
		}
	});
	auto pAnimation= new CAnimation();
	pAnimation->SetObj(this);
	pAnimation->AddClip("PlayerStop", AO_LOOP, 0.5f, 1, "Player", L"Player.bmp", "Player");
	pAnimation->AddClip("PlayerMove", AO_LOOP, 0.7f, 5, "PlayerMove", L"Move.bmp", "Player");
	pAnimation->AddClip("PlayerJump", AO_TAILSTOP, 0.75f, 4, "PlayerJump", L"Jump.bmp", "Player");
	if(pAnimation)
		SetAnimaton(pAnimation);
    return true;
}

CPlayer * CPlayer::Clone()
{
	return new CPlayer(*this);
}


int CPlayer::Update(float fDeltaTime)
{
	CMovingObj::Update(fDeltaTime);
	SetPhysics(true);
	if (KEYPRESS("Left")) {
		SetDir(-1.f);
		MoveXSpeed(fDeltaTime*GetDir());
		if (!GetJump())
			GetAnimation()->ChangeClip("PlayerMove");
	}
	if (KEYPRESS("Right")) {
		SetDir(1.f);
		MoveXSpeed(fDeltaTime*GetDir());
		if (!GetJump())
			GetAnimation()->ChangeClip("PlayerMove");
	}
	if(KEYUP("Left")||KEYUP("Right"))
		if(!GetJump())
			GetAnimation()->ChangeDefaultClip();
	if (KEYPRESS("Up"))
		Jump();
	if (KEYPRESS("Fire"))
		Fire();
	m_fReload += fDeltaTime;
	if (m_fReload >= m_fReloadTime)
		m_fReload = 0;
	if (GetLeftTop().x < 0)
		SetLTPos(0, GetLeftTop().y);
	if (GetPivotPos(1, 1).x >= GET_WORLD_RS.iW)
		SetLTPos(GET_WORLD_RS.iW - GetSize().x, GetLeftTop().y);
	if (GetLeftTop().y < 0)
		SetLTPos(GetLeftTop().x, 0);
	if (GetPivotPos(1, 1).y >= GET_WORLD_RS.iH)
		SetLTPos(GetLeftTop().x, GET_WORLD_RS.iH - GetSize().y);
	if (GetJump()) {
		MoveY(GetJumpSpeed(), fDeltaTime);
	}
	return 0;
}

int CPlayer::LateUpdate(float fDeltaTime)
{
	CMovingObj::LateUpdate(fDeltaTime);
	if (GetHP() <= 0) {
		Die();
		return 0;
	}
	return 0;
}

void CPlayer::Render(HDC hDC, float fDeltaTime)
{
	CMovingObj::Render(hDC, fDeltaTime);
	int iHP = GetHP();
	char buffer[1024] = {};
	string hp;
	_itoa_s(iHP, buffer, 10);
	hp = buffer;
	TextOutA(hDC, Left() + 10, Top() - 15, hp.c_str(), hp.size());
}
