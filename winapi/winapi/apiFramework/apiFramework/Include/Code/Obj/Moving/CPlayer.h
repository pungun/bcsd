#pragma once
#include "../CMovingObj.h"
class CPlayer :
    public CMovingObj
{
private:
	friend class CGObj;
private:
	float	m_fReload;
	float	m_fReloadTime;
	int		m_iHP;
	int		m_iPower;
	int		m_iBulletAmount;
	int		m_iMaxBullet;
	bool	m_bJump;
	float	m_fJumpSpeed;
private:
    CPlayer();
    CPlayer(const CPlayer& obj);
    ~CPlayer();
private:
	void Fire();
	void Jump();
public:
    bool Init();
	CPlayer* Clone();
	virtual int Update(float fDeltaTime);
	virtual int LateUpdate(float fDeltaTime);
    virtual void Render(HDC hDC, float fDeltaTime);
public:
	int GetHP() { return m_iHP; }
	void SetHP(int hp) { m_iHP = hp; }
	void ReviseHP(int value) { m_iHP += value; }
	int GetPower() { return m_iPower; }
	void SetPower(int power) { m_iPower = power; }
	void DeleteBullet() { --m_iBulletAmount; }
	bool GetJump() { return m_bJump;}
	void SetJump(bool jump) { m_bJump = jump; }
	float GetJumpSpeed() { return m_fJumpSpeed; }
	void SetJumpSpeed(float jumpspeed) { m_fJumpSpeed = jumpspeed; }
};

