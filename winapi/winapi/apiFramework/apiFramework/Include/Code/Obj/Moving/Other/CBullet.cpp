#include "CBullet.h"
#include "../../../Collision/CCollision.h"


CBullet::CBullet() :
	m_fDist(0.f),
	m_fLimitDist(0.f),
	m_fPower(0.f)
{
}

CBullet::CBullet(const CBullet & bullet)
{
	*this = bullet;
}

CBullet::~CBullet()
{
}

bool CBullet::Init()
{
	m_fDist=0.f;
	m_fLimitDist = 500.f;
	m_fPower = 100.f;
	return true;
}

int CBullet::Update(float fDeltaTime)
{
	CMovingObj::Update(fDeltaTime);
	m_fDist+= abs(MoveAngle(fDeltaTime));
	for (auto pColl : m_CollisionList)
		if (pColl->GetEnable())
			pColl->Update(fDeltaTime);
	if (m_fDist >= m_fLimitDist)
		Die();
	return 0;
}

void CBullet::Render(HDC hDC, float fDeltaTime)
{
	Ellipse(hDC, (int)Left(), (int)Top(), (int)Right(), (int)Bottom());
}

CBullet * CBullet::Clone()
{
	return new CBullet(*this);
}
