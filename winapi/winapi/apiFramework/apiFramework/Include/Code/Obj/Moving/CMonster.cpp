#include "CMonster.h"



CMonster::CMonster()
	:CMovingObj()
{
}

CMonster::CMonster(const CMonster & obj)
	:CMovingObj(obj)
{
	
}


CMonster * CMonster::Clone()
{
	return new CMonster(*this);
}

CMonster::~CMonster()
{
}

bool CMonster::Init()
{
	return true;
}

int CMonster::Update(float fDeltaTime)
{
	CMovingObj::Update(fDeltaTime);
	return 0;
}

void CMonster::Render(HDC hDC, float fDeltaTime)
{
	CMovingObj::Render(hDC,fDeltaTime);
}
