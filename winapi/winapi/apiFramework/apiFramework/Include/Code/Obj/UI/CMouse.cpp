#include "CMouse.h"
#include"../../Collision/CCollisionPoint.h"
#include"../../Core/Manager/CInput.h"
CMouse::CMouse():
	m_tMove(0.f,0.f)
{
}

CMouse::CMouse(const CMouse & obj):
	CUI(obj)
{
	m_tMove = obj.m_tMove;
}

CMouse::~CMouse()
{
}

CMouse * CMouse::Clone()
{
	return new CMouse(*this);
}

bool CMouse::Init()
{
	if (!CUI::Init())
		return false;
	SetSize(25.f, 25.f);
	SetTexture("Mouse", L"Mouse.bmp");
	auto pColl = AddCollision<CCollisionPoint>("Cursor");
	pColl->SetPos(GetPos());
	return true;
}

int CMouse::Update(float fDeltaTime)
{
	CUI::Update( fDeltaTime);
	return 0;
}
int CMouse::LateUpdate(float fDeltaTime)
{
	CUI::LateUpdate(fDeltaTime);
	return 0;
}
void CMouse::Render(HDC hDC, float fDeltaTime)
{
	CUI::Render(hDC, fDeltaTime);
}
