#include "CUI.h"
#include"../../Collision/CCollision.h"
CUI::CUI():
	m_tClientPos()
{
}

CUI::CUI(const CUI & obj):
	CGObj(obj)
{
	m_tClientPos = obj.m_tClientPos;
}

CUI::~CUI()
{
}

int CUI::Update(float fDeltaTime)
{
	CGObj::Update(fDeltaTime);
	SetPos(m_tClientPos+ GET_SINGLE(CCamera)->GetLeftTop());
	return 0;
}

int CUI::LateUpdate(float fDeltaTime)
{
	CGObj::LateUpdate(fDeltaTime);
	return 0;
}

void CUI::Render(HDC hDC, float fDeltaTime)
{
	if (m_pAnimation) {
		TransparentBlt(hDC, (int)GetClientLT().x, (int)GetClientLT().y, m_pAnimation->GetWidth(), m_pAnimation->GetHeight(), m_pAnimation->GetDC(), m_pAnimation->GetXStart(), 0, m_pAnimation->GetWidth(), m_pAnimation->GetHeight(), TRANS_COR);
	}
	else if (m_pTexture) {
		TransparentBlt(hDC, (int)GetClientLT().x, (int)GetClientLT().y, (int)m_tSize.x, (int)m_tSize.y, m_pTexture->GetDC(), 0, 0, (int)m_tSize.x, (int)m_tSize.y, TRANS_COR);
	}
	/*
	/* �浹ü ������
	auto iter = m_CollisionList.begin();
	while (iter != m_CollisionList.end()) {
		if (!(*iter)->GetLife()) {
			SAFE_RELEASE((*iter));
			iter = m_CollisionList.erase(iter);
			continue;
		}
		if ((*iter)->GetEnable())
			(*iter)->Render(hDC,fDeltaTime);
		++iter;
	}*/
}
