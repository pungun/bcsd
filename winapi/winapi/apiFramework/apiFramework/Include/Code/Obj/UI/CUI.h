#pragma once
#include "../CGObj.h"
class CUI :
	public CGObj
{
private:
	FPOS	m_tClientPos;
public:
	CUI();
	CUI(const CUI& obj);
	virtual ~CUI();
public:
	virtual CUI* Clone()=0;
public:
public:
	FPOS GetWorldPos() {

	}
public:
	virtual int Update(float fDeltaTime);
	virtual int LateUpdate(float fDeltaTime);
	virtual void Render(HDC hDC, float fDeltaTime);
public:
	FPOS GetClientPos() { return m_tClientPos; }
	void SetClientPos(FPOS pos) { 
		m_tClientPos = pos; 
		SetPos(m_tClientPos + GET_SINGLE(CCamera)->GetLeftTop());
	}
	FPOS GetClientLT() { return m_tClientPos - GetPivot() * GetSize(); }
};

