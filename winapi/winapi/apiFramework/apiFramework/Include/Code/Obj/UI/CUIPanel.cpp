#include "CUIPanel.h"

CUIPanel::CUIPanel()
{
}

CUIPanel::CUIPanel(const CUIPanel & obj):
	CUI(obj)
{
}

CUIPanel::~CUIPanel()
{
}

CUIPanel * CUIPanel::Clone()
{
	return new CUIPanel(*this);
}

bool CUIPanel::Init()
{
	if (!CUI::Init())
		return false;
	
	return true;
}

int CUIPanel::Update(float fDeltaTime)
{
	CUI::Update(fDeltaTime);
	return 0;
}

void CUIPanel::Render(HDC hDC, float fDeltaTime)
{
	CUI::Render(hDC, fDeltaTime);
}
