#pragma once
#include "CUI.h"
class CUIPanel :
	public CUI
{
private:

public:
	CUIPanel();
	CUIPanel(const CUIPanel& obj);
	virtual ~CUIPanel();
public:
	virtual CUIPanel* Clone();
public:
public:
public:
	virtual bool Init();
	virtual int Update(float fDeltaTime);
	virtual void Render(HDC hDC, float fDeltaTime);
};

