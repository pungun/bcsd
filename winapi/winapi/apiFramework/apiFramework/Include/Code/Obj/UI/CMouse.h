#pragma once
#include "CUI.h"
class CMouse :
	public CUI
{
private:
	friend class CGObj;
	friend class CScene;
private:
	FSIZE m_tMove;
public:
	CMouse();
	CMouse(const CMouse& obj);
	virtual ~CMouse();
public:
	virtual CMouse* Clone();
public:
	FSIZE GetMove() { return m_tMove; }
	void SetMove(FSIZE size) { m_tMove = size; }
	void SetMove(float x, float y) { SetMove(FSIZE(x,y)); }
public:
	virtual bool Init();
	virtual int Update(float fDeltaTime);
	virtual int LateUpdate(float fDeltaTime);
	virtual void Render(HDC hDC, float fDeltaTime);
};

