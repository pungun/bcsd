#include "CStaticObj.h"



CStaticObj::CStaticObj()
{
}

CStaticObj::CStaticObj(const CStaticObj & obj) :
	CGObj(obj)
{
}

CStaticObj::~CStaticObj()
{
}


int CStaticObj::Update(float fDeltaTime)
{
	CGObj::Update(fDeltaTime);
	return 0;
}

int CStaticObj::LateUpdate(float fDeltaTime)
{
	CGObj::LateUpdate(fDeltaTime);
	return 0;
}

void CStaticObj::Render(HDC hDC, float fDeltaTime)
{
	CGObj::Render(hDC, fDeltaTime);
}
