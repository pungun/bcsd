#pragma once
#include "CGObj.h"
class CMovingObj :
	public CGObj
{
private:
	float	m_fAngle;
	float	m_fSpeed;
	bool	m_bIsPhysics;
	float	m_fFallSpeed;
	float	m_fDir;

public:
	CMovingObj();
	CMovingObj(const CMovingObj& obj);
	virtual ~CMovingObj();
public:
	virtual CMovingObj* Clone()=0;
public:
	void Move(float x, float y);
	void Move(float x, float y, float fDeltaTime);
	void Move(const FPOS& tMove);
	void Move(const FPOS& tMove, float fDeltaTime);
	void MoveX(float x);
	void MoveX(float x, float fDeltaTime);
	void MoveXSpeed(float fDeltaTime);
	void MoveY(float y);
	void MoveY(float y, float fDeltaTime);
	void MoveYSpeed(float fDeltaTime);
	float MoveAngle();
	float MoveAngle(float fTime);
public:
	void SetAngle(float fAngle) {
		m_fAngle = fAngle;
	}
	float GetAngle(){
		return m_fAngle;
	}
	void SetSpeed(float fSpeed) {
		m_fSpeed = fSpeed;
	}
	float GetSpeed() {
		return m_fSpeed;
	}
	bool GetPhysics() { return m_bIsPhysics; }
	void SetPhysics(bool physics) { m_bIsPhysics = physics; }
	float GetFallSpeed() { return m_fFallSpeed; }
	void SetFallSpeed(float fallSpeed) { m_fFallSpeed= fallSpeed; }
	float GetDir() { return m_fDir; }
	void SetDir(float dir) { m_fDir = dir; }
public:
    virtual bool Init() = 0;
	virtual int Update(float fDeltaTime);
	virtual void Render(HDC hDC, float fDeltaTime);
};

