#include "CLayer.h"
#include "../Obj/CGObj.h"


CLayer::CLayer() :
	CReleaseable(),
    m_strTag(""),
    m_iZOrder(0),
    m_parentScene(nullptr)
{
}

CLayer::CLayer(const string & strTag, int ZOrder, const CScene * parentScene):
	CReleaseable(),
    m_strTag(strTag),
    m_iZOrder(ZOrder),
    m_parentScene((CScene*)parentScene)
{
}


CLayer::~CLayer()
{
	for (auto& element : m_ObjList)
		SAFE_RELEASE(element); 
	m_ObjList.clear();
	//SAFE_RELEASE_VEC_LIST(m_ObjList);
}

void CLayer::Update(float fDeltaTime)
{
	auto iter = m_ObjList.begin();
	auto iterEnd = m_ObjList.end();
	while (iter != iterEnd) {
		if (!(*iter)->GetLife()) {
			CGObj::EraseObj(*iter);
			SAFE_RELEASE((*iter));
			iter = m_ObjList.erase(iter);
			continue;
		}
		if ((*iter)->GetEnable())
			(*iter)->Update(fDeltaTime);
		++iter;
	}
}

void CLayer::LateUpdate(float fDeltaTime)
{
	auto iter = m_ObjList.begin();
	auto iterEnd = m_ObjList.end();
	while (iter != iterEnd) {
		if (!(*iter)->GetLife()) {
			CGObj::EraseObj(*iter);
			SAFE_RELEASE((*iter));
			iter = m_ObjList.erase(iter);
			continue;
		}
		if ((*iter)->GetEnable())
			(*iter)->LateUpdate(fDeltaTime);
		++iter;
	}
}


void CLayer::Render(HDC hDC, float fDeltaTime)
{
	auto iter = m_ObjList.begin();
	auto iterEnd = m_ObjList.end();
	while (iter != iterEnd) {
		if (!(*iter)->GetLife()) {
			CGObj::EraseObj(*iter);
			SAFE_RELEASE((*iter));
			iter = m_ObjList.erase(iter);
			continue;
		}
		if ((*iter)->GetEnable())
			(*iter)->Render(hDC,fDeltaTime);
		++iter;
	}
}

void CLayer::AddObject(CGObj * pGObj)
{
	pGObj->SetScene(m_parentScene);
	pGObj->SetLayer(this);
	if (pGObj->GetTag() == "MinionBullet")
		int i = 1;
	m_ObjList.push_back(pGObj);
	pGObj->AddRef();
}

