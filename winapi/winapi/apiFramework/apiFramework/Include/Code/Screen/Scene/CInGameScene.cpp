#include "CInGameScene.h"
#include"../../Obj/CGObj.h"
#include"../../Obj/Moving/Other/CBullet.h"
#include"../../Obj/Static/CStage.h"
#include"../../Obj/Moving/Monster/CMinion.h"
#include"../../Obj/Moving/CPlayer.h"
#include"../CLayer.h"
#include"../../Collision/CCollisionPixel.h"
#include"../../Obj/UI/CMouse.h"
CInGameScene::CInGameScene()
{
}

CInGameScene::CInGameScene(const CInGameScene & scene) :
	CScene(scene)
{
}


CInGameScene::~CInGameScene()
{

}

bool CInGameScene::Init()
{
    if (!CScene::Init())
        return false;
	auto* pStageLayer = FindLayer("Stage");
	auto* pStage = CGObj::CreateObj<CStage>("Stage", pStageLayer);
	auto* pPixelCollision = pStage->AddCollision<CCollisionPixel>("StageBody");
	pPixelCollision->SetPixelInfo(L"stage1.bmp");


	auto* pPlayer = CGObj::CreateObj<CPlayer>("Player", pStageLayer);
	auto* pMinion= CGObj::CreateObj<CMinion>("Minion", pStageLayer);
	pMinion->SetHP(1000);
	GET_SINGLE(CCamera)->SetTarget((CGObj*)pPlayer);
	//총알 프로토타입 생성
	auto* pBullet=CGObj::CreatePrototyle<CBullet>("Bullet");
	pBullet->SetSize(10.f, 10.f);
	pBullet->SetLimitDist(500.f);
	pBullet->SetPivot(0.f, 0.5f);
    return true;
}

void CInGameScene::Update(float fDeltaTime)
{
	CScene::Update(fDeltaTime);
}

void CInGameScene::LateUpdate(float fDeltaTime)
{
	CScene::LateUpdate(fDeltaTime);
}

void CInGameScene::Render(HDC hDC, float fDeltaTime)
{
	CScene::Render(hDC,fDeltaTime);
}
