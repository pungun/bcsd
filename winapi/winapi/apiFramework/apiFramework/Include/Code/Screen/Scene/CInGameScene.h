#pragma once
#include "../CScene.h"
class CInGameScene :
    public CScene
{
private:
    friend class CsceanManager;
public:
    CInGameScene();
    CInGameScene(const CInGameScene& scene);
    ~CInGameScene();
public:
	virtual bool Init();
	virtual void Update(float fDeltaTime);
	virtual void LateUpdate(float fDeltaTime);
	virtual void Render(HDC hDC, float fDeltaTime);
    
};

