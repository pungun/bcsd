#pragma once
#include "../CScene.h"
class CStartScene :
    public CScene
{
private:
    friend class CsceanManager;
public:
	CStartScene();
    ~CStartScene();
public:
	static class CStage* testStage;
	virtual bool Init();
	virtual void Update(float fDeltaTime);
	virtual void LateUpdate(float fDeltaTime);
	virtual void Render(HDC hDC, float fDeltaTime);
    
};

