#include"CStartScene..h"
#include"CInGameScene.h"
#include"CMapEditScene.h"
#include"../../Obj/CGObj.h"
#include"../../Obj/UI/CUIPanel.h"

#include"../../Obj/Static/CStage.h"
#include"../../Obj/Moving/Monster/CMinion.h"
#include"../../Obj/Moving/CPlayer.h"
#include"../CLayer.h"
#include"../../Collision/CCollisionRect.h"
#include"../../Obj/UI/CButton.h"
#include"../../Core/Manager/CInput.h"
CStartScene::CStartScene()
{
}


CStartScene::~CStartScene()
{

}

bool CStartScene::Init()
{
    if (!CScene::Init())
        return false;
	auto pHUDLayer = FindLayer("HUD");
	auto pUILayer = FindLayer("UI");
	auto pBackPanel = CGObj::CreateObj<CUIPanel>("BackPanel",pHUDLayer);
	pBackPanel->SetTexture("Intro", L"Start.bmp");
	pBackPanel->SetPos(0.f, 0.f);
	pBackPanel->SetSize(1280.f, 720.f);
	pBackPanel->SetPivot(FPOS(0.f, 0.f));

	auto pStartButton= CGObj::CreateObj<CButton>("StartButton", pUILayer);
	pStartButton->SetTexture("StartButton", L"Start.bmp", "Button");
	pStartButton->SetSize(80.f, 50.f);
	pStartButton->SetClientPos(FPOS(600.f, 310.f));
	auto pStartColl = pStartButton->AddCollision<CCollisionRect>("StartButtonBody");
	pStartColl->SetPos(pStartButton->GetPos());
	pStartColl->SetSize(pStartButton->GetSize());
	pStartColl->AddReact("Mouse");
	pStartColl->AddEvent([pStartColl](auto pColl) {
		if (KEYUP("MouseLClick")) {
			GET_SINGLE(CSceneManager)->CreateScene<CInGameScene>(SC_CURRENT);
			pStartColl->Disable();
		}
		});

	auto pMapEditButton = CGObj::CreateObj<CButton>("MapEditButton", pUILayer);
	pMapEditButton->SetTexture("MapEditButton", L"MapEdit.bmp", "Button");
	pMapEditButton->SetSize(80.f, 50.f);
	pMapEditButton->SetClientPos(FPOS(600.f, 365.f));
	auto pEditColl = pMapEditButton->AddCollision<CCollisionRect>("MapEditButtonBody");
	pEditColl->SetPos(pMapEditButton->GetPos());
	pEditColl->SetSize(pMapEditButton->GetSize());
	pEditColl->AddReact("Mouse");
	pEditColl->AddEvent([pStartColl](auto pColl) {
		if (KEYUP("MouseLClick")) {
			GET_SINGLE(CSceneManager)->CreateScene<CMapEditScene>(SC_CURRENT);
			pStartColl->Disable();
		}
		});
    return true;
}


void CStartScene::Update(float fDeltaTime)
{
	CScene::Update(fDeltaTime);
}

void CStartScene::LateUpdate(float fDeltaTime)
{
	CScene::Update(fDeltaTime);
}

void CStartScene::Render(HDC hDC, float fDeltaTime)
{
	CScene::Render(hDC, fDeltaTime);
}