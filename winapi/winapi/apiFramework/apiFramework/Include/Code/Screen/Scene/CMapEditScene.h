#pragma once
#include "../CScene.h"
class CMapEditScene :
    public CScene
{
private:
    friend class CsceanManager;
private:
	TILE_TYPE m_eSelecedTile;
public:
	CMapEditScene();
	CMapEditScene(const CMapEditScene& scene);
    ~CMapEditScene();
public:
	virtual bool Init();
	virtual void Update(float fDeltaTime);
	virtual void LateUpdate(float fDeltaTime);
	virtual void Render(HDC hDC, float fDeltaTime);
public:
	
    
};

