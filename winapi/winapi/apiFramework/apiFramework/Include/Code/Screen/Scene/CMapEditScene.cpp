#include "CMapEditScene.h"
#include "CIngameScene.h"
#include"../CLayer.h"

#include"../../Obj/CGObj.h"
#include"../../Obj/Moving/Other/CBullet.h"
#include"../../Obj/Moving/Monster/CMinion.h"
#include"../../Obj/Moving/CPlayer.h"
#include"../../Obj/Static/CStage.h"
#include"../../Obj/Static/CTileMap.h"
#include"../../Obj/Static/CTile.h"
#include"../../Obj/CInvisibleObj.h"
#include"../../Obj/UI/CUIPanel.h"
#include"../../Obj/UI/CMouse.h"
#include"../../Collision/CCollisionPixel.h"
#include"../../Collision/CCollisionRect.h"
#include"../../Core/Manager/CInput.h"

CMapEditScene::CMapEditScene():
	m_eSelecedTile(TT_DEFAULT)
{
}

CMapEditScene::CMapEditScene(const CMapEditScene & scene) :
	CScene(scene)
{
	m_eSelecedTile = scene.m_eSelecedTile;
}


CMapEditScene::~CMapEditScene()
{

}

bool CMapEditScene::Init()
{
    if (!CScene::Init())
        return false;
	auto pStageLayer = FindLayer("Stage");
	auto pStage = CGObj::CreateObj<CTileMap>("MapEditMap", pStageLayer);
	pStage->SetWidth(32);
	pStage->SetHeight(32);
	pStage->SetRow(100);
	pStage->SetColumn(100);
	for (int r = 0; r < pStage->GetRow(); ++r)
		for (int c = 0; c < pStage->GetColumn(); ++c)
			pStage->SetTile(r, c);
	GET_SINGLE(CCamera)->SetTarget(nullptr);
	GET_SINGLE(CCamera)->SetWorldResolution(pStage->GetSize().x, pStage->GetSize().y);

    return true;
}

void CMapEditScene::Update(float fDeltaTime)
{
	CScene::Update(fDeltaTime);
	if (KEYDOWN("F1"))
		m_eSelecedTile = TT_DEFAULT;
	else if (KEYDOWN("F2"))
		m_eSelecedTile = TT_GRASS;
	else if (KEYDOWN("F3"))
		m_eSelecedTile = TT_SAND;
	else if (KEYDOWN("F4"))
		m_eSelecedTile = TT_EARTH;
	else if (KEYDOWN("F5")) {
		CTileMap* pTileMap = (CTileMap*)(CGObj::FindObject("MapEditMap"));
		pTileMap->SaveMapFile(L"Map.tmf");
	}
	else if (KEYDOWN("F6")) {
		CTileMap* pTileMap = (CTileMap*)(CGObj::FindObject("MapEditMap"));
		pTileMap->LoadMapFile(L"Map.tmf");
		GET_SINGLE(CCamera)->SetWorldResolution(pTileMap->GetSize().x, pTileMap->GetSize().y);
	}
	else if (KEYDOWN("F7")) {
		GET_SINGLE(CSceneManager)->CreateScene<CInGameScene>(SC_CURRENT);
		return;
	}
	if (KEYPRESS("MouseLClick")) {
		CTileMap* pTileMap = (CTileMap*)(CGObj::FindObject("MapEditMap"));
		auto pos = GET_SINGLE(CInput)->GetMouse()->GetPos();
		CTile* pTile = nullptr;
		if(pTileMap->GetWidth()!=0&& pTileMap->GetHeight() != 0)
			pTile=pTileMap->GetTile((int)pos.y / pTileMap->GetHeight(), (int)pos.x / pTileMap->GetWidth());
		if (pTile) {
			pTile->SetType(m_eSelecedTile);
		}
	}
}

void CMapEditScene::LateUpdate(float fDeltaTime)
{
	CScene::LateUpdate(fDeltaTime);
}

void CMapEditScene::Render(HDC hDC, float fDeltaTime)
{
	CScene::Render(hDC,fDeltaTime);
}
