#pragma once
#include"../Core/Header/game.h"
#include"../Core/Manager/CReleaseable.h"
class CLayer
	:public CReleaseable
{
private:
    friend class CScene;
    class CScene*		m_parentScene;
    string				m_strTag;
    int					m_iZOrder;
    list<class CGObj*>	m_ObjList;
    CLayer();
    CLayer(const string& strTag, int ZOrder, const CScene* parentScene);
    ~CLayer();
public:
    void Update(float fDeltaTime);
    void LateUpdate(float fDeltaTime);
    void Render(HDC hDC, float fDeltaTime);
	void AddObject(class CGObj* pGObj);
//getter,setter
public:
    void SetTag(const string& strTag) {
        m_strTag = strTag;
    }
    string GetTag()const {
        return m_strTag;
    }
    void SetZOrder(int ZOrder) {
        m_iZOrder = ZOrder;
    }
    int GetZOrder()const {
        return m_iZOrder;
    }
    void SetParentScene(const CScene* parentScene ) {
        m_parentScene = (CScene*)parentScene;
    }
    CScene* GetParentScene() const {
        return m_parentScene;
    }
};

