#include "CScene.h"



bool CScene::Init() {
	CLayer* pLayer = CreateLayer("UI", INT_MAX);
	pLayer = CreateLayer("HUD", INT_MAX-1);
	pLayer = CreateLayer("Default", 1);
	pLayer = CreateLayer("Stage");
    return true;
}
CLayer * CScene::CreateLayer(const string & strTag, int iZOrder)
{
    auto pLayer = new CLayer(strTag,iZOrder,this);
    m_LayerList.push_back(pLayer);
	pLayer->AddRef();
    if(m_LayerList.size()>=2)
        m_LayerList.sort([](CLayer* l1, CLayer* l2)->bool {return l1->GetZOrder() < l2->GetZOrder(); });
    return pLayer;
}
CLayer * CScene::FindLayer(const string & strTag)
{
	for (auto layer : m_LayerList)
		if (layer->GetTag() == strTag)
			return layer;
	return nullptr;
}

void CScene::Update(float fDeltaTime)
{
    for (auto layer : m_LayerList) {
		if (layer->GetEnable())
			layer->Update(fDeltaTime);
    }
}
void CScene::LateUpdate(float fDeltaTime)
{
    for (auto layer : m_LayerList) {
		if (layer->GetEnable())
			layer->LateUpdate(fDeltaTime);
    }
}
void CScene::Render(HDC hDC, float fDeltaTime)
{
    for (auto layer : m_LayerList) {
		if (layer->GetEnable())
			layer->Render(hDC,fDeltaTime);
    }
}
CScene::CScene()
{
    
}

CScene::CScene(const CScene & scene)
{
	m_LayerList.clear();
	for (auto layer : scene.m_LayerList) {
		m_LayerList.push_back(layer);
		layer->AddRef();
	}
}

CScene::~CScene()
{
	SAFE_RELEASE_VEC_LIST(m_LayerList);
}
