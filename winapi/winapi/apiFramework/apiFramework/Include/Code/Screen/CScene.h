#pragma once
#include"../Core/Header/game.h"
#include"../Core/Manager/CSceneManager.h"
#include"CLayer.h"
class CScene
{
protected:
    friend class CSceneManager;
    list<CLayer*> m_LayerList;
protected:
    CScene();
	CScene(const CScene& scene);
    virtual ~CScene();
public:
    CLayer* CreateLayer(const string& strTag, int iZOrder=0);
	CLayer* FindLayer(const string& strTag);
    virtual bool Init();
    virtual void Update(float fDeltaTime);
    virtual void LateUpdate(float fDeltaTime);
    virtual void Render(HDC hDC, float fDeltaTime);
};

