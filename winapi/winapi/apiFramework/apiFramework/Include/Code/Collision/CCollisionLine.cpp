#include "CCollisionLine.h"
#include"../Core/Manager/CCollisionManager.h"
#include "CCollisionSphere.h"
#include "CCollisionRect.h"
#include "CCollisionPixel.h"
#include "CCollisionPoint.h"

CCollisionLine::CCollisionLine() :
	CCollision(),
	m_tLine({ 0,0 }, { 0,0 })
{
	m_eCollType = CT_LINE;
}
CCollisionLine::CCollisionLine(const CCollisionLine & coll) :
	CCollision(coll)
{
	m_tLine = coll.m_tLine;
}

CCollisionLine::~CCollisionLine()
{
}

CCollisionLine * CCollisionLine::Clone()
{
	return new CCollisionLine(*this);
}

bool CCollisionLine::Init()
{
	return true;
}

bool CCollisionLine::CollisionTo(CCollision * pColl)
{
	COLLISION(pColl)
}
