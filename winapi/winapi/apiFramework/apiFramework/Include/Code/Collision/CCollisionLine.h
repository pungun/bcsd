#pragma once
#include "CCollision.h"
class CCollisionLine :
	public CCollision
{
protected:
	friend class CGObj;
	CCollisionLine();
	CCollisionLine(const CCollisionLine& coll);
	~CCollisionLine();
private:
	FLINE m_tLine;
public:
	virtual CCollisionLine* Clone();
	virtual bool Init();
	virtual bool CollisionTo(CCollision* pColl);
public:
	FPOS GetBegin() { return m_tLine.begin; }
	void SetBegin(FPOS tBegin) { m_tLine.begin = tBegin; }
	void SetBegin(float x, float y) { SetBegin(FPOS(x, y)); }
	FPOS GetEnd() { return m_tLine.end; }
	void SetEnd(FPOS tEnd) { m_tLine.end = tEnd; }
	void SetEnd(float x, float y) { SetEnd(FPOS(x, y)); }
	FLINE GetLine() { return m_tLine; }
	void SetLine(FLINE tLine) { m_tLine = tLine; }

};
