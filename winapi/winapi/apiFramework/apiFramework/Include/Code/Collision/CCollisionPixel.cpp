#include "CCollisionPixel.h"
#include"../Core/Manager/CCollisionManager.h"
#include "CCollisionSphere.h"
#include "CCollisionLine.h"
#include "CCollisionRect.h"
#include "CCollisionPoint.h"

CCollisionPixel::CCollisionPixel() :
	CCollision(),
	m_iHeight(0),
	m_iWidth(0)
{
	m_eCollType = CT_PIXEL;
}
CCollisionPixel::CCollisionPixel(const CCollisionPixel & coll):
	CCollision(coll)
{
	m_iWidth = coll.m_iWidth;
	m_iHeight = coll.m_iHeight;
	m_vecPixel.clear();
	for (auto p : coll.m_vecPixel)
		m_vecPixel.push_back(p);
}

CCollisionPixel::~CCollisionPixel()
{
	SAFE_DELETE_VEC_LIST(m_vecPixel);
}

bool CCollisionPixel::SetPixelInfo(const wchar_t * pFileName, const string & strPathKey)
{
	const wchar_t* pPath=FIND_PATH(strPathKey);
	wstring wstrPath=pPath;
	wstrPath += pFileName;
	FILE* pFile = nullptr;
	_wfopen_s(&pFile, wstrPath.c_str(), L"rb");
	if (!pFile)
		return false;
	BITMAPFILEHEADER fh;
	BITMAPINFOHEADER ih;
	fread(&fh, sizeof(fh), 1, pFile);
	fread(&ih, sizeof(ih), 1, pFile);
	m_iWidth = ih.biWidth;
	m_iHeight = ih.biHeight;
	m_vecPixel.clear();
	for (int i = 0; i < m_iHeight; i++) {
		PIXELROW* pr=new PIXELROW;
		pr->row.resize(m_iWidth);
		m_vecPixel.push_back(pr);
	}
	for (auto& pr : m_vecPixel) {
		fread(&pr->row[0], sizeof(PIXEL), m_iWidth, pFile);
	}

	fclose(pFile);
	PIXELROW* PixelRow;
	for (int i = 0; i < m_iHeight / 2; ++i) {
		PixelRow = m_vecPixel[i];
		m_vecPixel[i] = m_vecPixel[m_iHeight - i - 1];
		m_vecPixel[m_iHeight - i - 1] = PixelRow;
	}
	return true;
}

CCollisionPixel * CCollisionPixel::Clone()
{
	return new CCollisionPixel(*this);
}

bool CCollisionPixel::Init()
{
	return true;
}

bool CCollisionPixel::CollisionTo(CCollision * pColl)
{
	COLLISION(pColl)
}
