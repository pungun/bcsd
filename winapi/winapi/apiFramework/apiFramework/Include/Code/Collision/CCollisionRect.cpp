#include "CCollisionRect.h"
#include"../Core/Manager/CCollisionManager.h"
#include"../Core/Manager/CCamera.h"
#include "CCollisionSphere.h"
#include "CCollisionLine.h"
#include "CCollisionPixel.h"
#include "CCollisionPoint.h"


CCollisionRect::CCollisionRect():
	CCollision(),
	m_tSize(0,0)
{
	m_eCollType = CT_RECT;
}

CCollisionRect::CCollisionRect(const CCollisionRect & coll):
	CCollision(coll)
{
	m_tSize = coll.m_tSize;
}


CCollisionRect::~CCollisionRect()
{
}

CCollisionRect*  CCollisionRect::Clone()
{
	return new CCollisionRect(*this);
}

bool CCollisionRect::Init()
{
	return true;
}

void CCollisionRect::Render(HDC hDC, float fDeltaTime)
{
	int left = (int)(GetPos().x - GET_SINGLE(CCamera)->GetLeftTop().x);
	int top = (int)(GetPos().y - GET_SINGLE(CCamera)->GetLeftTop().y);
	int right = left + (int)GetSize().x;
	int bottom = top + (int)GetSize().y;
	Rectangle(hDC, left, top, right, bottom);
}

bool CCollisionRect::CollisionTo(CCollision * pColl)
{
	COLLISION(pColl)
}
