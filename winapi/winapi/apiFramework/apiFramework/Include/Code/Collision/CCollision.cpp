#include "CCollision.h"
#include"../Obj/CGObj.h"


CCollision::CCollision():
	m_pObj(nullptr),
	m_tPos(0,0),
	m_tPrevPos(0,0),
	m_tRevision(0,0)
{
}

CCollision::CCollision(const CCollision & coll)
{
	*this = coll;
}


CCollision::~CCollision()
{
}

bool CCollision::Init()
{

	return true;
}


int CCollision::Update(float fDeltaTime)
{
	m_tPrevPos = m_tPos;
	m_tPos = m_pObj->GetLeftTop();
	return 0;
}

int CCollision::LateUpdate(float fDeltaTime)
{
	return 0;
}

void CCollision::Render(HDC hDC, float fDeltaTime)
{
}



