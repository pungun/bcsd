#include "CCollisionSphere.h"
#include"../Core/Manager/CCollisionManager.h"
#include "CCollisionRect.h"
#include "CCollisionLine.h"
#include "CCollisionPixel.h"
#include "CCollisionPoint.h"

CCollisionSphere::CCollisionSphere():
	CCollision(),
	m_fRadius(0)
{
	m_eCollType = CT_SPHERE;
}

CCollisionSphere::CCollisionSphere(const CCollisionSphere & coll):
	CCollision(coll)
{
	m_fRadius = coll.m_fRadius;
}


CCollisionSphere::~CCollisionSphere()
{
}

CCollisionSphere * CCollisionSphere::Clone()
{
	return new CCollisionSphere(*this);
}

bool CCollisionSphere::CollisionTo(CCollision * pColl)
{
	COLLISION(pColl)
}

void CCollisionSphere::Render(HDC hDC, float fDeltaTime)
{
	FPOS center = GetCenter();
	int left = (int)(GetCenter().x - GetRadius() - GET_SINGLE(CCamera)->GetLeftTop().x);
	int top = (int)(GetCenter().y - GetRadius() - GET_SINGLE(CCamera)->GetLeftTop().y);
	int right=(int)(GetCenter().x + GetRadius() - GET_SINGLE(CCamera)->GetLeftTop().x);
	int bottom=(int)(GetCenter().y + GetRadius() - GET_SINGLE(CCamera)->GetLeftTop().y);
	Ellipse(hDC, left, top,
		right,bottom );
}

bool CCollisionSphere::Init()
{
	return true;
}
