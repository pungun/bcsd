#pragma once
#include "../Core/Manager/CReleaseable.h"
#include "../Obj/CGObj.h"
using CollisionEvent = function<void(CCollision*)>;
class CCollision :
	public CReleaseable
{
protected:
	friend class CGObj;
	CCollision();
	CCollision(const CCollision& coll);
	virtual ~CCollision();

protected:
	COLLISION_TYPE		m_eCollType;
	string				m_strTag;
	class CGObj*		m_pObj;
	FPOS				m_tPos;
	FPOS				m_tPrevPos;
	FSIZE				m_tRevision;
	list<string>		m_ReactTagList;
	list<CollisionEvent>	m_EventList;
public:
	virtual CCollision* Clone()=0;
public:
	virtual bool Init();
	virtual int Update(float fDeltaTime);
	virtual int LateUpdate(float fDeltaTime);
	virtual void Render(HDC hDC, float fDeltaTime);
	virtual bool CollisionTo(CCollision* pColl)=0;
public:
	void AddEvent(CollisionEvent event) {
		m_EventList.push_back(event);
	}
	void ClearEvent() {
		m_EventList.clear();
	}
	void AddReact(const string& strTag) {
		m_ReactTagList.push_back(strTag);
	}
	void ClearReact() {
		m_ReactTagList.clear();
	}
	void RunEvent(CCollision* pColl) {
		for (auto event : m_EventList)
			if(pColl->GetEnable()&&GetEnable())
				event(pColl);
	}
public:
	//getter/setter
	COLLISION_TYPE GetCollType() const {
		return m_eCollType;
	}
	CGObj* GetObj()const {
		return m_pObj;
	}
	void SetObj(CGObj* obj) {
		m_pObj = obj;
	}
	FPOS GetPos() {
		return m_tPos;
	}
	void SetPos(FPOS tPos) {
		m_tPos = tPos;
	}
	void SetPos(float x, float y) {
		SetPos(FPOS(x, y));
	}
	FPOS GetPrevPos() {
		return m_tPrevPos;
	}
	FSIZE GetRevision() {
		return m_tRevision;
	}
	void SetRevision(FSIZE tSize) {
		m_tRevision = tSize;
	}
	void SetRevision(float x, float y) {
		m_tRevision = FSIZE(x, y);
	}
	string GetTag() { return m_strTag; }
	void SetTag(const string& strTag) { m_strTag = strTag; }
	bool HasNoTarget() { return m_ReactTagList.size() == 0; }
	bool IsReactTo(const string& strTag) {
		if (strTag == "Player") {
			int i = 0;
		}
		for (auto tag : m_ReactTagList)
			if (tag == strTag)
				return true;
		return false;
	}
};
