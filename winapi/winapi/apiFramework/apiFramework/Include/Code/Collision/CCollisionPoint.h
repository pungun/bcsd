#pragma once
#include "CCollision.h"
class CCollisionPoint :
	public CCollision
{
protected:
	friend class CGObj;
	CCollisionPoint();
	CCollisionPoint(const CCollisionPoint& coll);
	~CCollisionPoint();
public:
	virtual CCollisionPoint* Clone();
	virtual bool Init();
	virtual bool CollisionTo(CCollision* pColl);
};
