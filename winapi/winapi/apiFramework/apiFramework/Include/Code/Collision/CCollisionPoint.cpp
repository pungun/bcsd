#include "CCollisionPoint.h"
#include"../Core/Manager/CCollisionManager.h"
#include "CCollisionSphere.h"
#include "CCollisionRect.h"
#include "CCollisionPixel.h"
#include "CCollisionLine.h"


CCollisionPoint::CCollisionPoint() :
	CCollision()
{
	m_eCollType = CT_POINT;
}
CCollisionPoint::CCollisionPoint(const CCollisionPoint & coll) :
	CCollision(coll)
{
}

CCollisionPoint::~CCollisionPoint()
{
}

CCollisionPoint * CCollisionPoint::Clone()
{
	return new CCollisionPoint(*this);
}

bool CCollisionPoint::Init()
{
	return true;
}

bool CCollisionPoint::CollisionTo(CCollision * pColl)
{
	COLLISION(pColl)
}
