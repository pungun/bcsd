#pragma once
#include "CCollision.h"
#include "../Core/Manager/CPathManager.h"


class CCollisionPixel:
	public CCollision
{
protected:
	friend class CGObj;
	CCollisionPixel();
	CCollisionPixel(const CCollisionPixel& coll);
	~CCollisionPixel();
private:
	vector<PIXELROW*> m_vecPixel;
	int m_iWidth;
	int m_iHeight;
public:
	bool SetPixelInfo(const wchar_t* pFileName, const string& strPathKey = PATH_TEXTURE);
public:
	virtual CCollisionPixel* Clone();
	virtual bool Init();
	virtual bool CollisionTo(CCollision* pColl);
public:
	vector<PIXELROW*>* GetPixel() { return &m_vecPixel; }
	int GetWidth() { return m_iWidth; }
	int GetHeight() { return m_iHeight; }
};
