
#include "CCollision.h"
class CCollisionRect :
	public CCollision
{
protected:
	friend class CGObj;
	CCollisionRect();
	CCollisionRect(const CCollisionRect& coll);
	~CCollisionRect();
private:
	FSIZE	m_tSize;
public:
	virtual CCollisionRect* Clone();
	virtual bool Init();
	virtual void Render(HDC hDC, float fDeltaTime);
	virtual bool CollisionTo(CCollision* pColl);
public:
	void SetRect(FPOS pos, FSIZE size) {
		m_tPos = pos;
		m_tSize = size;
	}
	FSIZE GetSize() {
		return m_tSize;
	}
	void SetSize(FSIZE tSize){
		m_tSize = tSize;
	}
};
