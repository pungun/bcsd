
#include "CCollision.h"
class CCollisionSphere :
	public CCollision
{
protected:
	friend class CGObj;
	CCollisionSphere();
	CCollisionSphere(const CCollisionSphere& coll);
	~CCollisionSphere();
private:
	float m_fRadius;
public:
	virtual CCollisionSphere* Clone();
	virtual bool CollisionTo(CCollision* pColl);
	virtual void Render(HDC hDC, float fDeltaTime);
	virtual bool Init();
public:
	void SetRadius(float fRadius) {
		m_fRadius = fRadius;
	}
	float GetRadius() {
		return m_fRadius;
	}
	FPOS GetCenter() {
		return GetPos()+GetRevision()+GetRadius();
	}

};
