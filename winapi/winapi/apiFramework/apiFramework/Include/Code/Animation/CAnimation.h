#pragma once
#include "../Core/Manager/CReleaseable.h"
#include"../Core/Manager/CTexture.h"
class CAnimation :
	public CReleaseable
{
protected:
	friend class CGObj;

	unordered_map<string, pANIMATIONCLIP>	m_mapClip;
	pANIMATIONCLIP	m_pCurClip;
	string			m_strCurClip;
	string			m_strDefaultClip;
	CGObj*			m_pObj;
	bool			m_bMotionEnd;

	

	pANIMATIONCLIP FindClip(const string& strName);

public:
	CAnimation();
	CAnimation(const CAnimation& anim);
	~CAnimation();
	bool GetMotionEnd() const { return m_bMotionEnd; }
	HDC GetDC() { return m_pCurClip->pTexture->GetDC(); }
	int GetXStart() {
		return m_pCurClip->tFrameSize.cx*m_pCurClip->iFrame;
	}
	int GetWidth() { return m_pCurClip->tFrameSize.cx; }
	int GetHeight() { return m_pCurClip->tFrameSize.cy; }
	void SetType(ANIMATION_OPTION eOption) { m_pCurClip->eOption = eOption; }
	void SetObj(class CGObj* pObj) { m_pObj = pObj; }
	pANIMATIONCLIP GetCurrentClip() const { return m_pCurClip; }
	void SetCurrentClip(const string& strCurClip);
	void SetDefaultClip(const string& strDefaultClip);
	void ChangeClip(const string& strClip);
	void ChangeDefaultClip();

	bool Init();
	// Frame 형식으로 추가
	bool AddClip(const string& strName,	ANIMATION_OPTION eOption, float fAnimationLimitTime, int iLength,
		const string& strTexKey, const wchar_t* FileName, const string& strPathKey = PATH_TEXTURE);

	void Update(float fTime);
	CAnimation* Clone();
};

