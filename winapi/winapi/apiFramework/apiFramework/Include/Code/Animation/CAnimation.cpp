#include "CAnimation.h"
#include "../Core/Manager/CTexture.h"
#include "../Core/Manager/CResourceManager.h"
#include "../Obj/CGObj.h"

CAnimation::CAnimation() :
	m_pCurClip(NULL), m_bMotionEnd(false)
{
}

CAnimation::CAnimation(const CAnimation & anim)
{
	*this = anim;

	m_bMotionEnd = false;
	m_mapClip.clear();
	for (auto clip : anim.m_mapClip) {
		m_mapClip.insert(clip);
		clip.second->pTexture->AddRef();
	}

	m_pCurClip = nullptr;

	m_strCurClip = "";
	SetCurrentClip(anim.m_strCurClip);
}


CAnimation::~CAnimation()
{

	for (auto clip : m_mapClip) {
		SAFE_RELEASE(clip.second->pTexture);
		SAFE_DELETE(clip.second);
	}

	m_mapClip.clear();
}

pANIMATIONCLIP CAnimation::FindClip(const string & strName)
{
	auto iter = m_mapClip.find(strName);

	if (iter == m_mapClip.end())
		return nullptr;

	return iter->second;
}

void CAnimation::SetCurrentClip(const string & strCurClip)
{
	ChangeClip(strCurClip);
}

void CAnimation::SetDefaultClip(const string & strDefaultClip)
{
	m_strDefaultClip = strDefaultClip;
}

void CAnimation::ChangeClip(const string & strClip)
{
	if (m_strCurClip == strClip)
		return;

	m_strCurClip = strClip;

	if (m_pCurClip)
	{
		m_pCurClip->iFrame = 0;
		m_pCurClip->fAnimationTime = 0.f;
		m_pCurClip->fAnimationFrameTime = 0.f;
	}

	m_pCurClip = FindClip(strClip);
}

void CAnimation::ChangeDefaultClip()
{
	ChangeClip(m_strDefaultClip);
}


bool CAnimation::Init()
{
	return true;
}


bool CAnimation::AddClip(const string& strName,
	ANIMATION_OPTION eOption, float fAnimationLimitTime, int iLength,
	const string& strTexKey, const wchar_t* FileName,
	const string& strPathKey)
{
	pANIMATIONCLIP pClip = new ANIMATIONCLIP;

	pClip->eOption = eOption;
	pClip->fAnimationLimitTime = fAnimationLimitTime;
	pClip->iLength = iLength;
	pClip->fAnimationFrameLimitTime = fAnimationLimitTime / iLength;
	pClip->fAnimationFrameTime = 0.f;

	CTexture* pTex = GET_SINGLE(CResourceManager)->LoadTexture(strTexKey,FileName,strPathKey);

	pClip->pTexture = pTex;

	pClip->tFrameSize.cx = pClip->pTexture->GetInfo().bmWidth / iLength;
	pClip->tFrameSize.cy = pClip->pTexture->GetInfo().bmHeight;

	pClip->fAnimationTime = 0.f;
	pClip->iFrame=0;

	m_mapClip.insert(make_pair(strName, pClip));

	if (m_strDefaultClip.empty())
		SetDefaultClip(strName);

	if (m_strCurClip.empty())
		SetCurrentClip(strName);

	return true;
}

void CAnimation::Update(float fTime)
{
	m_bMotionEnd = false;
	m_pCurClip->fAnimationTime += fTime;
	m_pCurClip->fAnimationFrameTime += fTime;
	if (m_pCurClip->eOption == AO_TIMEOUT) {
		if (m_pCurClip->fAnimationTime >= m_pCurClip->fAnimationLimitTime) {
			ChangeDefaultClip();
			m_bMotionEnd = true;
			return;
		}
	}
	if (m_pCurClip->fAnimationFrameTime >= m_pCurClip->fAnimationFrameLimitTime) {
		if (m_pCurClip->iFrame == m_pCurClip->iLength - 1) {
			switch (m_pCurClip->eOption) {
			case AO_LOOP:
				m_pCurClip->iFrame = 0;
				m_pCurClip->fAnimationFrameTime = 0;
				m_bMotionEnd = true;
				return;
			case AO_ONCE:
				ChangeDefaultClip();
				return;
			case AO_TAILSTOP:
				m_bMotionEnd = true;
				return;
			}
		}
		m_pCurClip->iFrame += 1;
		m_pCurClip->fAnimationFrameTime = 0.f;
	}
}

CAnimation * CAnimation::Clone()
{
	return new CAnimation(*this);
}
